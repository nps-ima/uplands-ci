# Output metadata

-------------------------------------------------------------------------------

## Outputs root (`./`)
JAGS model file, calling script, and model summary.

| __File__ | __Description__ |
|---|---|
| *.jags | Model definition, the name for which follows the pattern '\<_likelihood_\>-\<_deterministic function_\>-\<_group-level effects_\>-\<_additional covariates_\>.jags' |
| calling-script.R | Reproducible calling script |
| mod-summary.csv | Test statistics (`p_sd`, `p_mean`), posterior predictive loss (`ppl`), deviance information criterion (`dic`), and $`\hat{R}`$ (`gelman_diag`) |
| vars-to-watch.txt | The names of variables to be monitored (and saved) by JAGS |
| inits-needed.txt | List of the parameters for which inits are required |
-------------------------------------------------------------------------------

## Analysis inputs (`./input/`)
Serialized R objects required to recreate analyses (JAGS inputs) and lookup tables.

| __File__ | __Description__ |
|---|---|
| jags-data.rds | A list containing the data |
| jags-info.rds | Data frame containing the JAGS and CODA variables to be monitored, in addition to several other 'environment' settings/variables used during the original pass through the analysis pipeline |
| jags-inits.rds | A list containing initial values |
| jags-n-iters.rds | The number of iterations used for adaptation, burn-in, and subsequent sampling |
| calendar-and-relative-years.csv | Mappings of calendar year to (zero-indexed) relative year of the study |
| site-ids-and-indices.csv | Mappings of site IDs (typicially character or non-sequential numeric IDs) to sequential indices |
| stratum-ids-and-indices.csv | Mappings of stratum IDs (stratum names) to their corresponding sequential indices |
| site-in-stratum-info.csv | Complete mappings of site IDs to indices, stratum IDs to stratum indices, and (most importantly, for our purposes) indices for sites within each stratum |
| state-variable-data-summary.txt | Used as a straight-face test for number of records, data types, etc. |
| state-variable-data.csv | The complete, pre-processed dataset used to construct many of the objects described above |

-------------------------------------------------------------------------------

## MCMC diagnostics (`./diagnostics/`)
Diagnostics are applied to monitor whether the Markov chains have converged.

| __File__ | __Description__ |
|---|---|
| convergence-diagnostics.txt | The Gelman and Rubin (1992) potential scale reduction statistic, $`\hat{R}`$, for selected parameters. $`\hat{R}`$ measures the ratio of the average variance of samples within each chain to the variance of samples pooled across all chains. If the chains have converged to a common distribution, $`\hat{R} \simeq 1`$, otherwise $`\hat{R} > 1`$. We hope to see $`\hat{R} < 1.1`$. |
| deviance-trace.png | Trace plot of parameter draws (in this case for deviance) |

-------------------------------------------------------------------------------

## Posterior predictive checks (`./ppc/`)
Graphical displays comparing the observed data to data simulated from the posterior predictive distribution. For mixed models, there will be files for each of the distributions. If the model fits well, we should be able to use it to generate data that looks a lot like the data we have in-hand.

| __File__ | __Description__ |
|---|---|
| y-rep-19-draws.png | Separate histograms of $`y`$ and some ($`n=19`$) of the $`y^{rep}`$ datasets |
| y-rep-stats.png | The distribution of test statistics. The solid, vertical line is the value of the test statistic computed from the observed data, $`y`$, while the histogram underlying this line represents the distribution of the test statistic in the $`y^{rep}`$ simulations. |
| y-rep-draws-df.rds |  |

-------------------------------------------------------------------------------

## Parameter estimates from MCMC draws (`./draws/`)
Tabular (and graphical--but not yet?) summaries of Markov chain Monte Carlo (MCMC) draws from the posterior distribution of the parameters of a Bayesian model.

| __File__ | __Description__ |
|---|---|
| coda-samples-summary.txt |  |

-------------------------------------------------------------------------------

## Status and trends inference (`./inference`)
Analysis outputs. The first asterisk corresponds to a match of either 'hat' or 'pred'.
The 'pred' files (_if_ present) correspond to predictions of site-, stratum-, and park-level means conditional on the actual time-varying values of covariates, while for 'hat' predictions any covariates present are held constant at their mean. Posterior uncertainty intervals (which are based on quantiles, and are sometimes called 'credible intervals') are provided both for the expected value (i.e., for the mean) of new observations as well as the value of new observations themselves. File extensions (the second asterisk) include '.rds', '.csv', and '.png'.

| __File__ | __Description__ |
|---|---|
| \*-roi-mu.\* | Park level mean of response |
| \*-stratum-means.\* |Stratum level mean of response  |
| \*-site-in-stratum-means.\* | Site level means of response |
| trend-perc-change-by-year.png | |
| trend-posteriors.csv |  |
| trend.png | Predicted annual change in mean of response |

Depending on the analysis, several other files may appear, including: '\*-site-means-(\<stratum-id\>).png', and 'pred-site-means-df.csv'.

-------------------------------------------------------------------------------

## Variography (`./variography`)

| __File__ | __Description__ |
|---|---|
| residuals-df.rds |  |
| variograms.png | Figure shows degree of spatial autocorrelation in data  |

-------------------------------------------------------------------------------

## Optional outputs (`./misc/`)
These may or may not be present.

| __File__ | __Description__ |
|---|---|
| z-jags.rds |  |
