library(tidyverse)
library(rstudioapi)
library(rjags)
library(coda)
library(magrittr)
load.module('dic')

source('src/fit-utils.r')
source('src/model-eval.R')
source('src/serializers.r')
source('src/ordinal-serializers.r')
source('src/theme.r')
source('src/apply-config.r')


# Set your working directory (works ONLY? if running interactively from RStudio).
path_to_outputs <- dirname(callFun("getActiveDocumentContext")$path)

# Load the tabular data, just for reference.
d <- read_csv(file.path(path_to_outputs, 'input', 'state-variable-data.csv'))

# Load the model description.
jags_model_file <- list.files(path_to_outputs, '*.jags$', full.names=TRUE)

# Load the data list required by JAGS.
jags_data <- readRDS(file.path(path_to_outputs, 'input', 'jags-data.rds'))

# Basic analysis breadcrumbs (likelihood, deterministic model, etc.).
jags_info <- readRDS(file.path(path_to_outputs, 'input', 'jags-info.rds'))
jags_n_iters <- readRDS(file.path(path_to_outputs, 'input', 'jags-n-iters.rds'))
eval_mean_for_tv_covariates <-  # determines whether 'bumpy' plots are produced
  readRDS(file.path(path_to_outputs, 'input', 'eval-mean-for-tv-covariates.rds'))

# Load the 'inits' originally used to fit the model.
jags_inits <- readRDS(file.path(path_to_outputs, 'input', 'jags-inits.rds'))

# Load the variables we're watching.
jags_vars <- readRDS(file.path(path_to_outputs, 'input', 'jags-vars.rds'))
coda_vars <- readRDS(file.path(path_to_outputs, 'input', 'coda-vars.rds'))

# Adapt and update.
jags_model <- jags.model(
  file=jags_model_file,
  data=jags_data,
  inits=jags_inits,
  n.chains=3,
  n.adapt=jags_n_iters['n_adapt']  # or something like 3000
  )
update(
  object=jags_model,
  n.iter=jags_n_iters['n_update']  # or something like 15000
  )

# Sample.
z_jags <- jags.samples(
  model=jags_model,
  variable.names=jags_vars,
  n.iter=jags_n_iters['n_iter']  # or something like 5000
  )

# Coda samples.
z_coda <- coda.samples(
  model=jags_model,
  variable.names=coda_vars,
  n.iter=jags_n_iters['n_iter']
  )
convergence_diagnostic <- gelman.diag(z_coda, multivariate=FALSE)
summary(z_coda)

# Posterior predictive loss, DIC, etc.
L <- get_L(jags_info)
post_pred_loss <- z_jags %>% get_ppl(d, L)
DIC <- z_jags %>% get_dic(d, L, jags_data)
mod_summary <- z_jags %>%
  get_mod_summary(post_pred_loss, DIC, convergence_diagnostic,
                  path_to_outputs)

# Outputs.
plot_bgcolor <- 'default'

a_label <- jags_info$description
a_deterministic_model <- jags_info$deterministic_model
this_L <- jags_info$likelihood

x_hat_raw <- jags_data$x.hat.raw
x_pred_raw <- jags_data$x.pred.raw

z_jags %T>%
  get_coef_post_dists(d, path_to_outputs, a_label, a_deterministic_model) %T>%
  {`if`(grepl('ordinal', this_L),
        get_site_inference_ord(., d, path_to_outputs, a_label,
                               x_hat_raw, 'hat', this_L), .)} %T>%
  {`if`(grepl('ordinal', this_L) & eval_mean_for_tv_covariates,
        get_site_inference_ord(., d, path_to_outputs, a_label,
                               unique(x_pred_raw), 'pred', this_L), .)} %T>%
  {`if`(grepl('hurdle-ordinal-latent-beta', this_L),
        get_stratum_inference_ord(., d, path_to_outputs, a_label,
                                  x_hat_raw, 'hat', this_L, n_draws=250), .)} %T>%
  {`if`(grepl('hurdle-ordinal-latent-beta', this_L) & eval_mean_for_tv_covariates,
        get_stratum_inference_ord(., d, path_to_outputs, a_label,
                                  unique(x_pred_raw), 'pred', this_L, n_draws=250), .)} %T>%
  get_residuals(d, this_L, path_to_outputs) %T>%  # was d_for_ppl
  get_y_reps(d, this_L, path_to_outputs, facet_by = NULL) %T>%
  get_trend_inference(d, path_to_outputs, a_label) %T>%
  {`if`(eval_mean_for_tv_covariates,
        get_park_scale_inference(., d, jags_data, path_to_outputs, a_label, #x.hat.raw, #'hat',
                                 n_draws = 500, seed=123), .)} %T>%
  get_site_inference(d, path_to_outputs, a_label, x_hat_raw, 'hat', this_L) %T>%
  {`if`(eval_mean_for_tv_covariates,
        get_site_inference(., d, path_to_outputs, a_label,
                           unique(x_pred_raw), 'pred', this_L), .)} %T>%
  {`if`(n_distinct(d$stratum_id) > 1,
        get_stratum_inference(., d, path_to_outputs, a_label,
                              x_hat_raw, 'hat', this_L, n_draws=250) , .)} %T>%
  {`if`(eval_mean_for_tv_covariates,
        get_stratum_inference(., d, path_to_outputs, a_label,
                              unique(x_pred_raw), 'pred', this_L, n_draws=250, x_hat_raw=x_hat_raw), .)}

# Trace plots. (replace deviance with another parameter if you like).
z_coda %T>% get_param_trace(path_to_outputs)

# z_jags %>% cache_inits(n_chains, path_to_outputs,
#                        chain_z = jags_inits[[1]]$z[1],
#                        converged)
