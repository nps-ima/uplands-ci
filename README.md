## uplands-ci

### Data
The data we are drawing on live (at least for the moment) in a Google Cloud
Storage bucket: <https://console.cloud.google.com/storage/browser/nps-ima>.
Data (in 'data.zip') have to be downloaded and unpacked at the root of the
project directory. If you're getting a permissions error, ensure that you're
signed in to Google Cloud Platform using the appropriate account (a CSP or NPS
email address). You can find this information by clicking the small person icon
(upper right) in your browser.

### Configuring analyses

Likewise, configuration (YAML) files for analyses of I&M program data are stored
in the bucket. An ASCII tree-style diagram for analyses of species richness data
at one network (ROMN), and one park unit (LIBI), can be found below. The `*.yml`
pattern is a placeholder for analyses of other, related sets of states variables
(cover or even ordinal variables, for example).

```
config/
└── ROMN/
    ├── LIBI/
    |   └── species-richness.yml
    │   └── *.yml
    │   └── _park-level-attributes.yml
    └── _network-level-attributes.yml
```

To minimize redundancies and opportunities for transcription errors,
configuration details, which populate each of the individual YAML files
above, are stored _throughout_ the hierarchy.
    Each I&M network uses different terminology to refer to the same thing. For
example, NCPN refers to their park units using the column 'Unit_Code', while
ROMN uses simply 'Park'. Attributes (column names) that are consistent across
all files for all park units within a given network are kept in
`_network-level-attributes.yml`. For instance, the suite of network-level
attributes for ROMN are specified as:

```yaml
unit code column: Park
site id column: SiteName
calendar year column: Year
```
In order to change aspects of an analysis (i.e. to add or change response
variables that are already included in your data file), you can edit the
appropriate YAML in our config directory. Open it in Atom or R studio, then add
the response column and description of the response variable you want to
include.
```yaml
response info:
    file: data/SCPN/modified/filtered-FunctionalGroupDataCSP.csv
    state variable:
        response column:
            - ForbCoverClass_10m
            - PerennialGrassCoverClass_10m
            **- ShrubCoverClass_10m**
        description:
            - Forb cover
            - Perennial grass cover
            **- Shrub cover**
    sampling method: transect
    sample id column(s):
        - Plot
        - Transect
        - Quadrat

site location info:
    file: data/SCPN/modified/PefoPlots.csv
    coordinate columns:
        - Quadrat_X
        - Quadrat_Y
```
You should now have the ability to run the analysis of shrub cover using the
pipeline.

Analyses can be configured to permit inference at the level of individual
strata or, alternatively, _across_ strata. An example of the latter can be
found in the tree beneath ROMN/LIBI. The file `_park-level-attributes.yml`
contains information about each of the three distinct strata at LIBI. The
stratum within which a site is located is given by the column `MDCATY`. The
total area (in this case, in m^2) and name of each stratum is provided in
key-value pairs nested beneath `stratum area info`:

```yaml
stratum id column: MDCATY
stratum area info:
    Gulley1: 684731
    Gulley2: 138247
    Upland: 1063552
```

These strata are 'non-ignorable' because samples were allocated to each stratum
independently and the number of samples in each stratum is not proportional to
the area of each stratum.

### Running analyses

To run the analyses defined in the config files, start the container with
```
$ ./run-shell.sh
```

Once in bash (running in the container), run an analysis utilizing 3 CPUs with,
for example
```
# ./analysis-pipeline.r \
    -f ./config/ROMN/GRKO/veg-categories-rel-cover.yml \
    -a 5000 -u 50000 -n 15000 \
    -c 3
```

The (optional) `-a`, `-u`, `-n`, and `-p` flags control the number of
iterations for adaptation, 'burnin', and sampling, and whether or not the
pipeline is executed in parallel, respectively. The defaults are 3K, 15K, and
5K iterations, and FALSE, respectively.

All outputs (including logs) are stored in the 'output' directory, which
mirrors &mdash; _almost_ exactly &mdash; the structure of the 'config'
directory. The only difference is that the results of analyses for each state
variable are stored separately, whereas the configuration of analyses for
like-variables (related count or cover variables, for example) can be specified
simultaneously in the same YAML file.

#### Model building
The shell script `compile-jags-file.sh` builds JAGS model files using several
arguments. The arguments &mdash; which correspond to the likelihood, the
deterministic function, group-level effects parameterization (random intercepts
and slopes vs. random intercepts only), the presence or absence of additional
covariates and, finally, the path to write to &mdash; are supplied via the
'MODEL' block in the YAML file for an analysis. For purposes of development or
debugging, `compile-jags-file.sh` can be sourced from the command line with an
optional output directory as such:
```
$ src/model-builder/compile-jags-file.sh \
    <likelihood> <deterministic-function> <group-level-effects> <covariates> \
    <write-path>
```
For example:
```
$ src/model-builder/compile-jags-file.sh \
    poisson exponential b0-b1 null \
    jags
```

The model compiler supports both random intercepts (`b0`) or random intercepts / random slopes (`b0-b1`) models with and without covariates for the following types of data:

| __Type of data__ | __Example__ | __Preferred / alternate statistical distribution(s)[^1]__ | __Deterministic model(s)__
| --- | --- | --- | --- |
| Counts[^2] | The number of native species on a 1 m^2 quadrat | `poisson` (`negative-binomial`) | `exponential`, `linear`, or `monomolecular` |
| Continuous and non-negative | Basal gap sizes along a transect | `lognormal` or `gamma` | `exponential` or `linear` |
| Presence-absence (zero _or_ one) | The occurrence (or not) of signs of human disturbance in a plot | `bernoulli` | `inverse-logit` |
| Counts in two categories | The number of point intercepts (_n_ = 100) at which non-native forbs are encountered along a transect | `binomial` or `beta-binomial` | `inverse-logit` |
| Proportion | A visual estimate of bare ground cover on a 1 m^2 quadrat | `beta` | `inverse-logit` |
| Ordinal | The cover class of forbs | `hurdle-ordinal-latent-beta`[^3] | `inverse-logit` |

[^1]: If posterior predictive checks indicate a lack of fit, alternate
probability models are provided to accommodate, e.g., overdispersed poisson
data (`negative-binomial`), right-skewed continuous data (`gamma`) and
overdispersed binomial data (`beta-binomial`).
[^2]: Models for counts are easily modified to accommodate zero-inflation with,
for example, a zero-inflated negative-binomial or gamma-Poisson mixture
parameterization.
[^3]: `hurdle-ordinal-latent-beta` is shorthand for a Bernoulli-beta hurdle model. It's 'ordinal-latent-beta' because the non-zero cover observations were made using cover classes, _not_ continuous ocular cover estimates. It models
zeros (no cover) and non-zeros (cover) as two separate processes. Thus, the
`hurdle-ordinal-latent-beta` model involves the use of two distinct likelihoods &mdash; a
Bernoulli for the presence or absence of the response (cover / no cover) and a
beta for the [_latent_](https://en.wikipedia.org/wiki/Latent_variable) cover
process (the conditional distribution of the positives after the 'hurdle' for
presence has been crossed).

#### Notes
Data collected along transects for the purposes of evaluating cover form
binomial trials &mdash; the number of 'successes' (hits) on a given number of
trials (point intercepts). As such, an additional entry for the name of the
column containing the number of trails is required.

We've created a ['cheatsheet'](https://docs.google.com/document/d/149LuTmPhF7gv-2-CaY8lxD3QhRcepH5GE6eoIIcM9HA/edit?usp=sharing) for each analysis that includes a breakdown of the
various JAGS object produced by each analysis, as well as some of the code
required to repeat a given analysis.
