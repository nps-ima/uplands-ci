#!/usr/bin/env Rscript

"
Compile the model summaries associated with a given run

Usage:
compile-results.r [-d <dir>]

Options:
-d The directory for which results should be compiled
" -> doc


args <- docopt::docopt(doc)

# Consolidate fits in a single file. Note that NAs in model checking attributes
# indicate failures.
source('src/wrangle.r')

args$d <- "output/MISC"

tmp_raw <- get_mod_summary_files(args$d, parse_coda_output = TRUE)
tmp <- tmp_raw %>%
  mutate(unit_code = str_extract(full_path, 'LAVO|SEKI|ROMO|CORI'),
         add_rand_coefs = ifelse(str_detect(full_path, '-new'), 'yes', 'no')) %>%
  rowwise() %>% mutate(outcome = na.omit(c(response, hits))) %>% ungroup()
# coefs <- lapply(tmp$full_path, function(x) {
#   tryCatch({
#     read_csv(file.path(x, '03-inference', 'coda-samples-quantiles.csv')) %>%
#       mutate(file = basename(x))
#   }, error = function(e) {
#     NULL
#   })
#
# })
# yep = tmp %>% 
#   filter(outcome == 'class', !grepl('deprec', full_path)) %>% 
#   mutate(updated = file.info(file.path(full_path, '01-diagnostics', 
#                                        'convergence-diagnostics.txt'))$ctime) %>% 
#   distinct(unit_code, var_type, var_level,  # likelihood, deterministic_model, 
#            add_rand_coefs, updated, full_path)

# read_csv('data/inits-cache/inits-file-lookup.csv') %>% 
#   filter(model %in% sprintf('./%s', yep %>% filter(is.na(updated)) %>% pull(full_path)))

passing_raw <- tmp %>% filter(gelman_diag_pt_est <= 1.1,
               p_sd >= 0.1, p_sd <= 0.9,
               p_mean >= 0.25, p_mean <= 0.75) %>%
  mutate(unit_code = str_extract(full_path, 'LAVO|SEKI|ROMO|CORI|TSRA'),
         add_rand_coefs = ifelse(str_detect(full_path, '-new'), 'yes', 'no')) %>%
  rowwise() %>%
  mutate(outcome = na.omit(c(response, hits))) %>%
  # mutate(outcome = across(any_of(c("response", "hits")), ~ na.omit(.))) %>%
  ungroup() %>%
  filter(!(!grepl('grazing.sign.any$', full_path) & unit_code %in% c("CORI", "ROMO")))
  # mutate(outcome = na.omit(c(response, hits))) %>% ungroup() %>%
  # arrange(outcome, unit_code) %>%
  # group_by(outcome, unit_code) %>% arrange(dic, ppl) %>% slice(1:3) %>% ungroup() %>% 

reruns <- passing_raw #%>% slice(1:2)
write_csv(reruns, 'sandbox/reruns.csv')

# # passing_raw <- tmp %>% filter(gelman_diag <= 1.5,
# #                p_sd >= 0.1, p_sd <= 0.9, # closer to zero is overdispersed
# #                p_mean >= 0.2, p_mean <= 0.8)
# 
# passing <- passing_raw %>%
#   # arrange(outcome, unit_code) %>%
#   group_by(outcome, unit_code) %>% arrange(ppl) %>% slice(1:3) %>% ungroup()
# 
# 
# # passing %>%
# #   select(outcome, unit_code, add_rand_coefs) %>%
# #   distinct() %>%
# #   filter(add_rand_coefs == 'no') %>%
# #   pivot_wider(names_from = unit_code, values_from = add_rand_coefs)
# 
# fun_range <- function(x, na_rm = TRUE) {
#   out <- (x - min(x, na.rm = na_rm)) /
#     (max(x, na.rm = na_rm) - min(x, na.rm = na_rm))
#   # browser()
#   if (all(out < 0, na.rm = TRUE)) return(numeric(length(out)))
#   if (all(is.nan(out)) & length(out) == 1) return(0)
#   out
# }
passing_best <- passing_raw %>%
  group_by(outcome, unit_code) %>%
  # Criteria
  # mutate(crit = (abs(p_mean - 0.5) + abs(p_sd - 0.5)) / 2 +
  #          fun_range(dic) + fun_range(ppl)) %>%  # abs(p_mean - 0.5)
  # arrange(crit) %>% slice(1:3) %>%
  arrange(ppl) %>% slice(1) %>%
  ungroup()
passing_best %>%
  # distinct(outcome, unit_code, add_rand_coefs) %>%
  group_by(outcome, unit_code) %>%
  summarise(add_rand_coefs = paste(add_rand_coefs, collapse = '/'),
            .groups = 'drop') %>%
  pivot_wider(names_from = unit_code, values_from = add_rand_coefs) %>% 
  select(outcome, CORI, ROMO, SEKI, LAVO)

# passing %>% filter(unit_code == "SEKI", hits == "bare_ground") %>% View()

# reruns <- passing# %>% slice(1:2)
# write_csv(reruns, 'sandbox/reruns.csv')

# passing %>% select(response, hits, full_path) %>% arrange(full_path) %>% View()

lapply(passing$full_path, function(x) {
  x_new <- sub('output/MISC/', 'output/MISC-passing/', x)
  # dir.create(x_new, showWarnings = FALSE, recursive = FALSE)
  # file.copy(x, dirname(x_new), recursive = TRUE)
  R.utils::copyDirectory(x, x_new)
})

# bind_rows(coefs) %>% filter(grepl("Beta", param))

# print(args$d)
combine_mod_summaries <- function(folder, add_dir_metadata = FALSE,
                                  write_summaries = FALSE) {

  mod_summaries_files <-
    list.files(path=folder,
               pattern="^mod-summaries", recursive = TRUE, full.names = TRUE)
  mod_summaries_list <- lapply(mod_summaries_files, function(x) {
    if(add_dir_metadata) {
      dir_metadata <- strsplit(x, '/|//')[[1]]
      read_csv(x) %>%
        mutate(network = dir_metadata[2], park = dir_metadata[3])
    } else {
      read_csv(x)
    }
  })
  if(write_summaries) {
    do.call(bind_rows, mod_summaries_list) %T>%
      write_csv(file.path(folder, 'combined-mod-summaries.csv'))
  } else {
    do.call(bind_rows, mod_summaries_list)
  }

}

# tmp <- combine_mod_summaries('output', add_dir_metadata = TRUE)
# write_csv(tmp, 'output/every-model-summary.csv')

# tmp %>% group_by(response, hits, trials)
# # View(tmp %>% select(-full_path) %>% distinct)
# keepers <- tmp %>%
#   filter(gelman_diag < 2) %>%
#   mutate(sh_cmd = paste('mkdir -p', sub('output', 'output-keepers', full_path), '&&',
#                         'cp -r', full_path, sub('output', 'output-keepers', full_path))) %>%
#   pull(sh_cmd)
# for(keeper in keepers) {
#   system(keeper)
# }
