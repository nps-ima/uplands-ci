
get_stratum_inference_no_cis <- function(mcarray_list, data, folder, resp_var_desc,
                                  timesteps, jags_obj_prefix, likelihood,
                                  wrap_char_n=15, x_hat_raw = timesteps,
                                  ...) {
  print('Inference on strata (no cis)...')
  if (!sprintf('%s.strat.mean', jags_obj_prefix) %in% names(mcarray_list)) return(NULL)

  show_preds_against_data <- show_data_in_output(mcarray_list)
  if(likelihood == 'hurdle-ordinal-latent-beta') show_preds_against_data <- FALSE
  # Preliminaries.
  # strat_mean_suffixes <- if(jags_obj_prefix == 'hat') {
  #   c('', '.oos')
  # } else {
  #   ''
  # }
  oos_entry <- ifelse(any(grepl(sprintf('%s.strat.*oos$', jags_obj_prefix),
                                names(mcarray_list))), '.oos', NA)
  tmp = grep('*.oos$', names(mcarray_list), value = TRUE)
  oos_entry <- ifelse(all(tmp %in% 'hat.strat.eta.oos'), NA, '.oos') # temporary until i can find this object!
  strat_mean_suffixes <- na.omit(c('', oos_entry))
  in_and_out <- lapply(strat_mean_suffixes, function(strat_mean_suffix) {
    # browser()
    sms_dash <- sub('\\.', '-', strat_mean_suffix)
    sm_sub_dir <- ifelse(grepl('oos', strat_mean_suffix), 'all', 'sampled')
    is_beta_binomial <- grepl('beta-binomial', likelihood)
    jags_obj_suffix <- 'mean'  #ifelse(is_beta_binomial, 'p', 'mean')
    stratum_means_obj <- paste0(
      paste(jags_obj_prefix, 'strat', jags_obj_suffix, sep='.'), strat_mean_suffix)
    stratum_lookup <- get_stratum_lookup(data)
    stratum_mu_full <- concat_chains(mcarray_list[[stratum_means_obj]], axis=4)
    # browser()
    year_vec <- data %>% get_year_lookup(timesteps) %>% pull(year)
    # browser()
    new_obs_obj <- paste(jags_obj_prefix, 'strat.new.obs', sep = '.')
    # browser()
    show_preds_against_data <-
      if(new_obs_obj %in% names(mcarray_list) & !grepl('oos', strat_mean_suffix)) {
        show_preds_against_data
      } else {
        FALSE
      }

    x_index <- 0
    n_strata <- dim(mcarray_list[[stratum_means_obj]])[2]
    stratum_mu_subset <- stratum_mu_full %>% thin_mcmc_output(axis=3, ...)
    stratum_mu_draws <- lapply(seq_len(n_strata), function(x) {
      x_index <<- x_index + 1
      # browser()
      this_stratum_mu_subset <- if(n_strata == 1) {
        stratum_mu_subset
      } else {
        stratum_mu_subset[, x, ]
      }
      this_stratum_mu_subset %>%
        as_tibble(.name_repair = v_digit) %>%
        mutate(year=year_vec, stratum_index=x_index) %>%
        left_join(stratum_lookup) %>%
        gather(k, stratum_mu, -year, -matches('stratum'))
    }) %>% bind_rows()
    # browser()
    stratum_mean_mu <- summary(mcarray_list[[stratum_means_obj]], mean)$stat %>%
      as_tibble(.name_repair = NULL) %>%
      mutate(year=year_vec) %>%
      gather(stratum_col, stratum_mean_mu, -year) %>%
      left_join(stratum_lookup)

    fncols <- function(data, cname) {
      # https://stackoverflow.com/questions/45857787/adding-column-if-it-does-not-exist
      add <-cname[!cname%in%names(data)]

      if(length(add)!=0) data[add] <- 1
      data
    }
    #browser()
    stratum_data <- data %>%
      select(year=cal_year, stratum_id, outcome=matches("^response$|^hits$"),
             normalizer = matches('trials')) %>%
      fncols('normalizer') %>%
      mutate(outcome = outcome / normalizer)

    # Plotting.
    y_lev <- 'Stratum-level'
    get_y_lab <- function(showing_y_new) {
      if(showing_y_new) {
        as.expression(
          bquote(atop(.(y_lev), .(tolower(resp_var_desc)) ~
                        (mu * ',' ~ italic(y)^new))))
      } else {
        as.expression(
          bquote(atop(.(y_lev), .(tolower(resp_var_desc)) ~ (mu))))
      }

    }
    lapply(show_preds_against_data, function(with_data) {
      ggplot() +
        facet_grid(.~stratum_id, labeller = label_wrap_gen(wrap_char_n)) +
        geom_line(data=stratum_mu_draws, aes(x=year, y=stratum_mu, group=k),
                  color='blue', alpha=.01) +
        geom_line(data=stratum_mean_mu, aes(x=year, y=stratum_mean_mu),
                  color=ima_post_col(plot_bgcolor)[2], size=1.01) +
        # Add layer with actual data.
        {
          if(with_data) {
            geom_point(data=stratum_data, aes(x=year, y=outcome),
                       color=ima_post_col(plot_bgcolor)[2], alpha=.25,
                         position=position_jitter(
                           width = diff(range(stratum_data$year)) * .01,
                           height = diff(range(stratum_data$outcome, na.rm = TRUE)) * .01
                         ))  # width = 0.1, height = 0.1
            } else {
              NULL
            }
          } +
        labs(x='Year', y=get_y_lab(with_data)) +
        scale_x_continuous(breaks=scales::pretty_breaks()) +
        theme_ima(plot_bgcolor) #+
        # lims(y = quantile(stratum_mu_draws$stratum_mu, probs = c(0.001, 0.999)))

      ggsave(file.path(folder, '03-inference/strata', sm_sub_dir,
                       paste0(jags_obj_prefix, '-stratum-means',
                              ifelse(with_data, '-with-data', ''),
                              sms_dash, '-no-cis.png')),
             width = n_distinct(data$stratum_id) * 4, height = 5)
    })

  })

}


y_rep_obj_name <- function(likelihood) {
  "y.rep"
}

get_op_plots_new <- function(mcarray_list, data, folder) {

  if (!any(grepl("^y.rep$", names(mcarray_list)))) return(NULL)
  output_dir <- file.path(folder, '02-checking/op')
  dir.create(output_dir, showWarnings = FALSE, recursive = TRUE)

  y_rep_obj <- "y.rep"

  y_rep_stats <- apply(concat_chains(mcarray_list[[y_rep_obj]], 3), 1, quantile,
                       probs = c(0.025, 0.5, 0.975))
  d_op <- t(y_rep_stats) %>% as_tibble() %>%
    mutate(y = data %>% select(matches('hits|response')) %>% pull(1))

  rmse <- sqrt(sum(d_op$y - d_op$`50%`) / nrow(d_op))

  ggplot(d_op) +
    geom_abline(intercept = 0, slope = 1, color = 'red', linetype = 'dashed') +
    geom_segment(aes(x = `2.5%`, xend = `97.5%`, y = y, yend = y), alpha = 0.5) +
    geom_point(aes(x = `50%`, y = y), pch = 21, size = 2, fill = 'white') +
    geom_label(x = Inf, y = -Inf, hjust = 'inward', vjust = 'inward',
               label = sprintf("RMSE: %s", round(rmse, 3))) +
    labs(x = "Predicted", y = "Observed") +
    theme_ima(plot_bgcolor, rotate_y_lab=FALSE)
  ggsave(file.path(output_dir, 'op.jpg'), width = 7, height = 5)
}

get_op_plots <- function(mcarray_list, data, likelihood, folder,
                       bayesian_p_summary, ...) {

  # Pulls yreps (used for model checking).
  # Data simulated using the model (1 for each draw).

  y_rep_name <- y_rep_obj_name(likelihood)
  n_draws <- mcarray_list %>% y_rep_n_iter(likelihood)

  y_rep_type <- gsub('[.]', '-', y_rep_name)
  get_y_rep_draws <- function(.mcarray_list, .data, .y_rep_name, ...) {

    # The following two control flow measures are only really invoked if a
    # hurdle model has been passed to this function.
    hurdle_filter <- ifelse(.y_rep_name=='y.rep.beta',
                            'outcome>0', 'outcome==outcome')
    hurdle_mutate <- ifelse(.y_rep_name=='y.rep.bern',
                            'ifelse(outcome>0, 0, 1)', 'outcome=outcome')
    browser()
    okthen <- t(apply(concat_chains(.mcarray_list[[.y_rep_name]], axis=3), MARGIN = 1,
          quantile, probs = c(0.025, 0.5, 0.975))) %>%
      as_tibble(.name_repair = NULL) %>%
      bind_cols(data)
    okthen2 <- concat_chains(.mcarray_list[[.y_rep_name]], axis=3)[sample(1:3540, 50), ] %>%
      as_tibble(.name_repair = NULL) %>%
      mutate(observation_index = 1:n()) %>%
      gather(k, outcome, -observation_index) %>%
      left_join(data %>% transmute(observation_index = 1:n(), act_outcome = response))
    ggplot(okthen2 %>% arrange(outcome), aes(x = outcome, y = factor(observation_index), height = ..density..)) +
      geom_density_ridges(stat  ='binline', binwidth = 1, draw_baseline = F, alpha = .5)
      # gather(k, outcome, -observation_index) %>%
      # bind_rows(.data %>%
      #             select(outcome=matches('response|hits')) %>%
      #             mutate(k='Actual') %>%
      #             mutate_(.dots=setNames(hurdle_mutate, 'outcome'))) %>%
      # mutate(grp=ifelse(str_detect(k, '^V'), 'Replicate', 'Actual')) %>%
      # filter_(hurdle_filter)
    ggplot(okthen %>% sample_n(2)) + # %>% sample_n(1000)
      geom_point(aes(x = response, y = `50%`), alpha = 1) +
      geom_errorbar(aes(ymin = `2.5%`, ymax = `97.5%`, x = response), alpha = 1) +
      # geom_abline(intercept = 0, slope = 1, color = 'blue') +
      labs(x = 'Observed', y = 'Predicted')  # TODO: calculate RMSE?

    # ggplot(iris, aes(x = Sepal.Length, y = Species)) + geom_density_ridges()
  }
  y_reps_draws_list <- list()
  y_reps_draws_list[[1]] <- get_y_rep_draws(mcarray_list, data, y_rep_name,
                                            n_draws = n_draws, ...)
  browser()
  # data %>% mutate()
  y_reps_draws_list[[1]]

  if(y_rep_name =='y.rep.beta') {
    y_rep_type <- c(y_rep_type, 'y-rep-bern')
    y_reps_draws_list[[2]] <- get_y_rep_draws(mcarray_list, data, 'y.rep.bern',
                                              n_draws = n_draws, ...) %T>%
      saveRDS(file.path(folder, '02-checking/ppc', 'y-rep-bern-draws-df.rds'))
  }
  # Plot the distributions of 19 such replicated datasets against the actual
  # data.
  lapply(seq_along(y_rep_type), function(x) {
    x_axis_position <- if(grepl('bern', y_rep_type[x])) {
      scale_x_discrete(limit=c(0, 1), labels=c('Present', 'Absent'))
    } else {
      NULL
    }

    ggplot(y_reps_draws_list[[x]] %>%
             mutate(draw_index=group_indices(., k, grp)) %>%
             filter(draw_index<=20)) +
      facet_wrap(~interaction(k, grp)) +
      geom_histogram(aes(x=outcome, y=..density.., fill=grp),
                     color='black', size=.05) +
      scale_fill_manual('Group', labels=c(expression(italic(y)),
                                          expression(italic(y)^rep)),
                        values=ima_pal(plot_bgcolor)[c(1, 2)]) +
      theme(strip.background = element_blank(),
            strip.text.x = element_blank(),
            axis.text.y=element_blank(),
            axis.ticks.y=element_blank()) +
      x_axis_position +
      labs(x=expression(italic(y)), y=expression('['*italic(y)*']')) +
      theme_ima(plot_bgcolor, rotate_y_lab=TRUE)
    ggsave(file.path(folder, '02-checking/ppc', paste(y_rep_type[x], '19-draws.png', sep='-')),
           width = 9, height = 9)
  })

  # Visualize the mean and variance of the actual data against the
  # distribution of means and variances of the replicated datasets.
  y_reps_stats <- y_reps_draws_list[[1]] %>%
    group_by(k, grp) %>%
    summarise(mu=mean(outcome), sigma=sd(outcome)) %>%
    ungroup %>%
    gather(stat, value, -k, -grp)
  p_val_labels <- bayesian_p_summary %>%
    select(matches('^p_')) %>%
    gather(key, p_val) %>%
    mutate(stat=ifelse(grepl('_mean', key), 'mu', 'sigma'))
  actual_values <- y_reps_stats %>%
    filter(grp=='Actual') %>%
    left_join(p_val_labels)
  ggplot() +
    facet_wrap(~stat, scales='free', labeller=label_parsed) +
    theme(strip.text.x = element_text(size = 14)) +
    geom_histogram(data=y_reps_stats %>% filter(grp=='Replicate'),
                   aes(x=value, y=..density..), fill=ima_pal(plot_bgcolor)[2],
                   color='black', size=.1) +
    geom_vline(data=actual_values,
               aes(xintercept=value), color=ima_pal(plot_bgcolor)[1], size=1.1) +
    geom_label(data=actual_values, aes(x=value, y=0, label=p_val)) +
    labs(x='', y='Relative frequency') +
    theme_ima(plot_bgcolor)
  ggsave(file.path(folder, '02-checking/ppc', 'y-rep-stats.png'), width = 7, height = 5)

}
