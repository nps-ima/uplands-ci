library(multidplyr)


cluster_init <- function(cores) {
  new_cluster(cores) %>%
    cluster_library(c('tidyverse', 'magrittr')) %>% 
    cluster_copy(c('n_adapt', 'n_update', 'n_iter', 'plot_bgcolor',
                   'rowwise_fit', 'config_list', 'prep_for_hpc'))
    # cluster_copy(n_adapt) %>% 
    # cluster_copy(n_update) %>% 
    # cluster_copy(n_iter) %>% 
    # cluster_copy(plot_bgcolor) %>% 
    # cluster_copy(rowwise_fit) %>% 
    # cluster_copy(config_list) %>% 
    # cluster_copy(prep_for_hpc)
}
