library(hrbrthemes)
library(mapview)
library(leaflet)
# library(leaflet.extras)
# library(leaflet.providers)
library(ggridges)
if (!file.exists('bin/phantomjs')) webshot::install_phantomjs()
source('src/utils-mcmc.R')

get_cnh_maps <- function(mcarray_list, data, folder, resp_var_desc) {

  # debugging...
  # folder <- "output/MISC/LAVO/ground-cover-new/bare-ground-cover/od-binom_inv-logit_b0_fixed-stratum-var_rand-point_jurisdiction-effects/elev_point_jurisdiction-deflections_fire.sign.any_human.disturbance.any_forest.mgmt.any"
  # mcarray_list <- readRDS(file.path(folder, '99-misc', 'z-jags.rds'))
  # resp_var_desc <- readRDS(file.path(folder, '00-input', 'jags-info.rds'))$description

  maps_dir <- file.path(folder, '99-misc/maps')
  dir.create(maps_dir, showWarnings = FALSE, recursive = TRUE)

  y_rep_mean <- summary(mcarray_list$y.hat, FUN = 'mean')$stat
  d_raw <- data %>% # read_csv(file.path(folder, '00-input', 'state-variable-data.csv')) %>%
    left_join(
      read_csv(file.path(folder, '00-input', 'encoding-for-point_jurisdiction.csv'))
    ) %>%
    mutate(y_rep_mean) %>%
    mutate(point = str_extract(sample_id, ".*(?=-)"), obs_idx = 1:n())

  d <- d_raw %>%
    group_by(site_id) %>%
    mutate(contrast = paste(sort(unique(point_jurisdiction)), collapse = '-')) %>%
    ungroup()

  d_most <- d %>%
    group_by(site_id, point_jurisdiction, contrast) %>%
    summarise(mean_y_rep_mean = mean(y_rep_mean), .groups = 'drop') %>%
    group_by(site_id, contrast) %>%
    arrange(point_jurisdiction) %>%
    summarise(diff_mean_y_rep_mean = diff(mean_y_rep_mean), .groups = 'drop') %>%
    group_by(contrast) %>%
    arrange(desc(abs(diff_mean_y_rep_mean))) %>%
    slice(c(1, n())) %>%
    mutate(most = c('dissimilar', 'similar')) %>%
    ungroup()
  write_csv(d_most, file.path(maps_dir, 'in-situ-preds.csv'))

  sampled_contrasts <- d %>%
    distinct(site_id, contrast) %>%
    group_by(contrast) %>%
    tally()
  write_csv(sampled_contrasts, file.path(maps_dir, 'sampled-contrasts.csv'))

  # mapping stuff
  d_coords <- read_csv('data/MISC/d-coords-combined.csv')
    #read_excel('data/MISC/seki_lavo_field_data_6_2_21.xlsx') %>%
    #select(site_id = Site, point = Point, unit_code = PACE, x = xcoords,
    #       y = ycoords, jurisdiction = POINT_J)
  sf_coords <- st_transform(st_as_sf(d_coords, coords = c("x", "y"),
                                     crs = 5070),
                            crs = 4326)
  pal <- colorFactor("Set1", domain = sf_coords$jurisdiction)

  # ridges stuff
  y_hat_mat <- concat_chains(mcarray_list$y.hat, axis = 3)
  dimnames(y_hat_mat) <-
    list(obs_idx = 1:dim(y_hat_mat)[1], iter = 1:dim(y_hat_mat)[2])
  y_hat <- as.data.frame.table(y_hat_mat, responseName = "y_hat") %>%
    as_tibble() %>%
    mutate(across(everything(), as.numeric))
  d_y_hat <- d_raw %>% left_join(y_hat)

  lapply(d_most$site_id, function(this_site) {
    message(this_site)
    d_most_current <- d_most %>% filter(site_id == this_site)

    # ---- map ----
    m1 <- {
      leaflet(sf_coords %>% filter(site_id == d_most_current$site_id),
              options = leafletOptions(zoomControl = FALSE)) %>%
        addProviderTiles(providers$Esri.WorldImagery, group = 'True color') %>%
        addCircleMarkers(
          weight = 0.5,
          col = 'black',
          fillColor = ~pal(jurisdiction),
          fillOpacity = 0.9,
          stroke = T,
          label = ~point,
          labelOptions = labelOptions(noHide = TRUE, textOnly = TRUE, #textsize = "15px",
                                      direction = "top",
                                      style = list(
                                        "color" = "black",
                                        "font-style" = "bold",
                                        "text-shadow" = "1px 1px #ffffff",
                                        "font-size" = "16px"
                                      )),
          group = 'Points') %>%
        addLegend(pal = pal, values = ~jurisdiction, opacity = 0.7, #title = NULL,
                  position = "topright", title = sprintf("Jurisdictions (site %s)", d_most_current$site_id))
    }
    # browser()
    # m1
    contrasts_dir <- file.path(maps_dir, d_most_current$contrast)
    dir.create(contrasts_dir, showWarnings = FALSE, recursive = TRUE)
    m1_filename <- file.path("~", contrasts_dir, sprintf('most-%s-map.jpg', d_most_current$most)) #d_most_current
    mapshot(m1, file = m1_filename) # , vwidth = 700, vheight = 500
    att <- paste0("<a href='https://www.usgs.gov/'>",
                  "U.S. Geological Survey</a> | ",
                  "<a href='https://www.usgs.gov/laws/policies_notices.html'>",
                  "Policies</a>")
    GetURL <- function(service, host = "basemap.nationalmap.gov") {
      sprintf("https://%s/arcgis/services/%s/MapServer/WmsServer", host, service)
    }
    m2 <- m1 %>%
      # addProviderTiles(providers$NASAGIBS.ModisTerraBands367CR, group = "NASA") %>%
      # leaflet::addWMSTiles(map, GetURL("USGSImageryOnly"),
      #                      group = "NASA", attribution = att, layers = "0") %>%
      addMiniMap(tiles = providers$Esri.NatGeoWorldMap,#providers$CartoDB.Positron,
                 position = 'topleft',
                 width = 250, height = 250,
                 toggleDisplay = FALSE) %>%
      addScaleBar() %>% leafem::addHomeButton(group = "Points") #%>%
    # addLayersControl(baseGroups = c("True color", "NASA"),
    #                  options = layersControlOptions(collapsed = FALSE))
    m2
    mapshot(m2, url = str_replace(m1_filename, '.jpg$', '.html'))

    # ---- ridges ----
    d_yaya <- d_y_hat %>% filter(site_id == d_most_current$site_id) %>%
      left_join(sf_coords %>% mutate(col = pal(jurisdiction)) %>% as_tibble())

    col_table <- d_yaya %>% distinct(jurisdiction, col)
    p1 <- ggplot(d_yaya) +
      geom_density_ridges(aes(y = fct_rev(point), x = y_hat, group = sample_id,
                              fill = jurisdiction),
                          alpha = 0.5) +
      scale_fill_manual("Jurisdiction", values = col_table$col, breaks = col_table$jurisdiction) +
      # scale_fill_identity() +
      theme_ipsum_rc(grid = "Y", #base_family = font_an,
                     base_size = 14,
                     axis_title_size = 16, strip_text_size = 16) +
      labs(x = sprintf('Mean %s', tolower(resp_var_desc)), y = "Point",
           title = sprintf("Site %s", d_most_current$site_id)) +
      theme(legend.position = 'bottom') +
      geom_point(data = d %>%
                   select(site_id, sample_id, unit_code,
                          outcome = matches('hits|response'), point) %>%
                   filter(site_id == d_most_current$site_id),
                 aes(x = outcome, y = point, group = sample_id))
    # p1
    p1_filename <- file.path(contrasts_dir, sprintf('most-%s-ridges.jpg', d_most_current$most))
    ggsave(p1_filename, plot = p1, width = 7, height = 5)

  })
}
