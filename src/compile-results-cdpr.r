#!/usr/bin/env Rscript

"
Compile the model summaries associated with a given run

Usage:
compile-results.r [-d <dir>]

Options:
-d The directory for which results should be compiled
" -> doc


args <- docopt::docopt(doc)

# Consolidate fits in a single file. Note that NAs in model checking attributes
# indicate failures.
source('src/wrangle.r')

args$d = "output/CDPR/TSRA"
tmp_raw <- get_mod_summary_files(args$d, parse_coda_output = TRUE)

tmp <- tmp_raw %>%
  mutate(unit_code = str_extract(full_path, 'TSRA'),
         add_rand_coefs = ifelse(str_detect(full_path, '-new'), 'yes', 'no')) %>%
  rowwise() %>% mutate(outcome = na.omit(c(response, hits))) %>% ungroup()

# reruns <- passing 
# write_csv(reruns, 'sandbox/reruns.csv')

passing_raw <- tmp %>% filter(gelman_diag_pt_est <= 1.1, #gelman_diag <= 1.25,
               p_sd >= 0.1, p_sd <= 0.9, # closer to zero is overdispersed
               p_mean >= 0.2, p_mean <= 0.8)

passing_best <- passing_raw %>%
  group_by(outcome, unit_code) %>%
  arrange(ppl) %>% slice(1) %>%
  ungroup()
passing_best
passing_best %>% 
  mutate(is_done = file.exists(file.path(full_path, '99-misc', 
                                         'control_vs_burn', 'pred-site-mean-across-parks.jpg'))) %>% 
  filter(is_done)

# saveRDS(unique(passing_best$full_path, readRDS('sandbox/cdpr-finished.rds')), 
#         'sandbox/cdpr-finished.rds')

newones <- setdiff(passing_best$full_path, readRDS('sandbox/cdpr-finished.rds'))

lapply(newones[c(3, 5)], function(x) { # was: passing_best$full_path
  x_new <- sub('output/CDPR/', 'output/CDPR-passing/', x)
  # dir.exists(x_new)
  # dir.create(x_new, showWarnings = FALSE, recursive = FALSE)
  # file.copy(x, dirname(x_new), recursive = TRUE)
  R.utils::copyDirectory(x, x_new)
})


# bind_rows(coefs) %>% filter(grepl("Beta", param))

# print(args$d)
combine_mod_summaries <- function(folder, add_dir_metadata = FALSE,
                                  write_summaries = FALSE) {

  mod_summaries_files <-
    list.files(path=folder,
               pattern="^mod-summaries", recursive = TRUE, full.names = TRUE)
  mod_summaries_list <- lapply(mod_summaries_files, function(x) {
    if(add_dir_metadata) {
      dir_metadata <- strsplit(x, '/|//')[[1]]
      read_csv(x) %>%
        mutate(network = dir_metadata[2], park = dir_metadata[3])
    } else {
      read_csv(x)
    }
  })
  if(write_summaries) {
    do.call(bind_rows, mod_summaries_list) %T>%
      write_csv(file.path(folder, 'combined-mod-summaries.csv'))
  } else {
    do.call(bind_rows, mod_summaries_list)
  }

}

# tmp <- combine_mod_summaries('output', add_dir_metadata = TRUE)
# write_csv(tmp, 'output/every-model-summary.csv')

# tmp %>% group_by(response, hits, trials)
# # View(tmp %>% select(-full_path) %>% distinct)
# keepers <- tmp %>%
#   filter(gelman_diag < 2) %>%
#   mutate(sh_cmd = paste('mkdir -p', sub('output', 'output-keepers', full_path), '&&',
#                         'cp -r', full_path, sub('output', 'output-keepers', full_path))) %>%
#   pull(sh_cmd)
# for(keeper in keepers) {
#   system(keeper)
# }
