library(tidyverse)
library(readxl)
library(mapview)
library(lubridate)

source('src/cnh-utils.r')


# PACE: protected area-centered ecosystems
#   - ROMO: Rocky Mountain National Park
#   - CORI: Colorado River (intended to be the PACE surrounding Grand Canyon)
#   - Sequoia/Kings Canyon
#   - Lassen
#   - Great Smoky Mountain
# boundary comparisons: NPS/USFS Nonwilderness; NPS/USFS Wilderness; NPS/BLM; BLM/USFS Nonwilderness; BLM USFS Wilderness; USFS Wilderness/USFS Nonwildernes

d_path <- "data/MISC/CNH_MASTERDATA_2018_fixed.xlsx"
excel_sheets(d_path)

parse_sample_info <- function(data, sample_info_attr) {
  data %>% 
    mutate(site = str_extract(!! sample_info_attr, "[:digit:]+(?=_?)"),
           point = str_extract(!! sample_info_attr, "[:alpha:]+?(?=_?)"),
           PACE = str_extract(!! sample_info_attr, "(?<=_).*"),
           PACE = getPACE(PACE))
}

d_plots <- read_excel(d_path, "General Plot Stats") %>%
  parse_sample_info(quo(`PLOT NUMBER`)) %>% 
  mutate(date = ymd(DATE), year = year(date)) #%>% 
  # bind_rows(tibble(site = '40', point = "A", PACE = "CORI", date = ymd('2018-05-16'), year = 2018))

d_plot_locs <- d_plots %>% 
  select(site, point, PACE, lat = Latitude, lon = Longitude)
write_csv(d_plot_locs, 'data/MISC/CNH2018/d-plot-locs.csv')

source('src/cnh-utils.r')
d_plot_drivers <- d_plots %>% 
  select(site, point, PACE, date, year,
         point_jurisdiction = `POINT JURISDICTION`, 
         bordering_jurisdiction = `BORDERING JURISDICTION`,
         point_complexity = Point_Complexity, 
         point_divergence = Point_Divergence,
         point_dc = Point_DC,
         elev = Elevation, slope = SLOPE, aspect = ASPECT) %>% 
  mutate(#northness = cos(as.numeric(aspect) * pi / 180),
         mandate_contrast = paste(get_mandate(point_jurisdiction),
                               get_mandate(bordering_jurisdiction),
                               sep = '-'),
         mandate = get_mandate(point_jurisdiction)) %>%
  group_by(site, PACE) %>% 
  mutate(elev = ifelse(is.na(elev), mean(elev, na.rm = TRUE), elev)) %>% 
  ungroup() %>% 
  select(-aspect) 
d_plot_drivers <- d_plot_drivers %>% 
  bind_rows(
    # Crudely impute site info for point A at site 40 in CORI, which was missing.
    d_plot_drivers %>% filter(site == '40', PACE == "CORI", point_jurisdiction == 'BLM') %>% mutate(point = 'A')
  )
# browser()
write_csv(d_plot_drivers, 'data/MISC/CNH2018/d-plot-drivers.csv')

if (FALSE) {
  d_plots %>% 
    group_by(PACE) %>% 
    summarise(n_points = n_distinct(point), n_sites = n_distinct(site))
  
  d_plots %>% 
    group_by(site, point, PACE) %>% 
    summarise(n_visits = n_distinct(DATE)) %>% 
    ungroup()
  
  d_plots %>% 
    group_by(`POINT JURISDICTION`, `BORDERING JURISDICTION`) %>% 
    tally() %>% 
    ungroup() %>% 
    arrange(`POINT JURISDICTION`, `BORDERING JURISDICTION`)
  
  mapview(d_plots %>% filter(!is.na(Longitude)), 
          xcol = "Longitude", ycol = "Latitude", crs = 4326,
          zcol = 'PACE')
}

d_lpi_binary <- read_excel(d_path, "LPI Transects") %>% 
  parse_sample_info(quo(`Site/Point/PACE`)) %>% 
  select(-`Site/Point/PACE`, -`...5`) %>% 
  distinct() %>% 
  gather(transect, cover, -`Line intercept distance`, -site, -point, -PACE) %>% 
  mutate(transect = as.numeric(str_extract(transect, '\\d+')),
         cover = get_groundcover_code(cover)) %>% 
  filter(!is.na(cover))



d_lpi_binomial <- d_lpi_binary %>% 
  group_by(site, point, PACE, transect, cover) %>% 
  summarise(n_hits = n()) %>% 
  ungroup() %>% 
  spread(cover, n_hits, fill = 0) %>% 
  left_join(
    d_lpi_binary %>% 
      group_by(site, point, PACE, transect) %>% 
      summarise(n_trials = n()) %>% 
      ungroup()
  ) %>% 
  left_join(d_plot_drivers %>% select(site, point, PACE, date)) %>% 
  mutate(stratum = 'none')
write_csv(d_lpi_binomial %>% na.omit, 'data/MISC/CNH2018/d-lpi-binomial.csv')

d_plot_drivers_bonus <- d_plot_drivers %>%
  select(site, point, PACE, elev, point_jurisdiction, date, year) %>%
  right_join(select(d_lpi_binomial, -date)) %>%
  # filter(site == 40)
  mutate(site = as.double(site)) %>% 
  select(site, point, PACE, transect, bare_ground, grass, shrub, tree, stratum, 
         elev, point_jurisdiction, date, year) %>% 
  na.omit
write_csv(d_plot_drivers_bonus, 'data/MISC/CNH2018/d-plot-drivers-bonus.csv')

# ---- enhance d_plot_drivers data ----

field_data_veg = read_excel('data/MISC/fied_data_veg.xlsx') %>% 
  parse_sample_info(quo(PLOT_NUMBE)) %>% 
  mutate(PACE = as.character(PACE))

# field_data_veg %>% 
#   filter(PACE == "CORI") %>% 
#   # group_by(EVT_PHYS) %>% tally %>% 
#   group_by(EVT_LF) %>% 
#   tally  # TODO: try this!
field_data_evt <- field_data_veg %>% 
  mutate(
    EVT_LF = ifelse(EVT_LF %in% c('Barren', 'Developed', 'Sparse'), 
                    'Unproductive', EVT_LF)
  ) %>% 
  select(site, point, PACE, EVT_LF)


d_plot_drivers %>% 
  left_join(field_data_evt) %>% 
  mutate(EVT_LF = ifelse(site == 63 & point == 'C', 
                         'Tree', EVT_LF)) #%>% 
  # left_join(d_lpi_binomial %>% select(site, point, PACE, ??))
  # filter(PACE == 'CORI') %>% 
  # group_by(EVT_LF) %>% tally

# ---- disturbance info ----
excel_sheets(d_path)
# d_disturbance_deprec <- read_excel(d_path, "Summary_Disturbance") %>%
#   parse_sample_info(quo(SITE)) %>% 
#   select(-c(1:14), transect = TRANSECT) %>% 
#   mutate(n_trials = 50, stratum = 'none') %>% 
#   left_join(d_plot_drivers %>% select(site, point, PACE, date))
# d_disturbance <- 
caster <- function(x, y) as.integer(str_detect(x, sprintf('\\b%s\\b', y)))
d_disturbance <- read_excel(d_path, "DISTURBANCE") %>%
  parse_sample_info(quo(`Site/Point/PACE`)) %>% 
  select(where(~!all(is.na(.x))), -`...7`, -`Site/Point/PACE`) %>% # remove all purely NA cols
  left_join(d_plot_drivers %>% select(site, point, PACE, date)) %>% 
  pivot_longer(matches("^Transect"), names_to = 'transect', values_to = 'code') %>% 
  mutate(n_trials = 1, transect = as.integer(str_extract(transect, '\\d')),
         CH = caster(code, "CH"), 
         FL = caster(code, "F\\;|FL"), 
         FLB = caster(code, "BE\\. FLB|CLB|FB|FLS|FLB"),
         RD = caster(code, "ATV|PAVED ROAD|TIRE MARKS|DIRT ROAD|AIRSTRIP"), 
         TR = caster(code, "TR"), #TRASH = caster(code, "TRASH"),
         FF = caster(code, "FF"),
         OTHER_HUMAN_DISTURBANCE = caster(code, "MACHINERY|BW"),
         CATTLE.SCAT = caster(code, "CATTLE|CATTLE SCAT\\. GT|SCAT \\(CAT\\)|CATTLE SCAT"),
         CATTLE.PRINT = caster(code, "CATTLE PRINT")) %>% 
  # filter(grepl('CATTLE SCAT', code)) %>% View()
  rowwise() %>% 
  # ---- NEW ----
  mutate(
    GRAZING.SIGN.ANY = as.integer(any(CATTLE.PRINT > 0, CATTLE.SCAT > 0)),
    FIRE.SIGN.ANY = FF, # FF: fire footprint (primarily charring, ash, etc.)
    HUMAN.DISTURBANCE.ANY = as.integer(any(RD > 0, TR > 0, CH > 0, OTHER_HUMAN_DISTURBANCE > 0)), # BW is barbwire; we leave TRASH out here
    FOREST.MGMT.ANY = as.integer(any(CH > 0, FL > 0, FLB > 0)) # FL: fallen log, FLB: fallen log base
  ) %>%
  ungroup() %>% 
  select(-matches('^Belt'), - code) %>%
  group_by(site, point, PACE, date, transect) %>%
  summarise(across(everything(), sum), .groups = 'drop') %>%
  mutate(stratum = 'none',
         GRAZING.SIGN.ANY.01 = ifelse(GRAZING.SIGN.ANY > 0, 1, 0),
         FIRE.SIGN.ANY.01 = ifelse(FIRE.SIGN.ANY > 0, 1, 0),
         HUMAN.DISTURBANCE.ANY.01 = ifelse(HUMAN.DISTURBANCE.ANY > 0, 1, 0),
         FOREST.MGMT.ANY.01 = ifelse(FOREST.MGMT.ANY > 0, 1, 0))
d_disturbance
# tmp %>% select(matches('\\.ANY$')) %>% cor
# lapply(tmp, function(x) strsplit(x, ',')) %>% unlist() %>% trimws() %>% table() %>% sort

  mutate(n_trials = 50, stratum = 'none') %>% 
  left_join(d_plot_drivers %>% select(site, point, PACE, date))
# TODO: add any (not all) cattle sign (scat or print)
# names(d_disturbance) <- sub(" ", ".", names(d_disturbance))
write_csv(d_disturbance %>% na.omit, 'data/MISC/CNH2018/d-disturbance.csv')

head(d_disturbance)
# disturbance_patterns <- 
#   paste(paste(c("GRAZING", "FIRE", "HUMAN", "FOREST"), "ANY", sep = ".*"), collapse = "|")
# d_disturbance_signs <- 
# na.omit(d_disturbance) %>% 

  # select(site, point, PACE, transect, stratum, matches(disturbance_patterns),
  #        n_trials, date)
  # select(-Site, -Point, -Notes, -`Site/Point/PACE`, -`Type...27`, -`Growth...33`,
  #        -`Transect Start (m)`, -`Transect End (m)`, -Date, transect = Transect,
  #        -date_raw)
  # group_by(site, point, PACE, date, transect) %>%
  # summarise(across(everything(), sum), .groups = 'drop') 
# write_csv(d_disturbance_signs, 'data/MISC/CNH2018/d-disturbance-signs.csv')

d_plot_drivers_w_disturbance <- d_plot_drivers %>%
  select(site, point, PACE, elev, point_jurisdiction, date, year) %>%
  right_join(d_disturbance) %>%
  mutate(site = as.double(site)) %>% 
  select(site, point, PACE, transect, matches('GRAZING|FIRE|HUMAN|FOREST'), stratum, 
         elev, point_jurisdiction, date, year) %>% 
  na.omit
write_csv(d_plot_drivers_w_disturbance, 'data/MISC/CNH2018/d-plot-drivers-w-disturbance.csv')

# ---- soils info ----
d_soil_stability <- read_excel(d_path, 'Soil Stability Test') %>% 
  parse_sample_info(quo(`Site/Point/PACE`))
# d_soil_stability <- read_excel(d_path, 'Soilstab') %>% 
#   parse_sample_info(quo(`Site/Point/PACE`))
d_soil_stability <- d_soil_stability %>% 
  mutate(class = as.numeric(Class), class = ifelse(class > 6, NA, class),
         stratum = 'none', transect = `Trans #`, date = ymd(Date))
unique(d_soil_stability$class)  # Q: okay to remove 'NA'-like records
hist(d_soil_stability$class)
d_soil_stability_final <- d_soil_stability %>% 
  filter(!is.na(d_soil_stability$class)) %>% 
  select(site, point, PACE, date, class, stratum, transect)
write_csv(d_soil_stability_final, 'data/MISC/CNH2018/d-soil-stability.csv')

# P-M ppm (which is parts per million) and percent C should be switched but just the headings

romo_soil_chem <- read_excel(d_path, 'Soil_chemistry') %>% 
  parse_sample_info(quo(plot)) %>% 
  filter(PACE == "ROMO") %>% 
  mutate(transect = as.integer(str_extract(Transect, '\\d'))) %>% 
  select(site, point, PACE, TotalN = `Total N\r\n %`, TotalC = `Total C\r\n %`, 
         PMppm = `P-M\r\n ppm`, NO3Nppm = `NO3-N\r\n ppm`, NH4Nppm = `NH4-N\r\n ppm`,
         transect) %>% 
  mutate(across(all_of(c("TotalN", "TotalC", "PMppm", "NO3Nppm", "NH4Nppm")),
                       as.double)) %>% 
  mutate(across(all_of(c("TotalN", "TotalC")), ~.x / 100)) %>% 
  drop_na()
# romo_soil_chem <- read_excel('data/MISC/CNH_ROMO_Soils_022718.xlsx') %>% 
#   parse_sample_info(quo(`...10`)) %>% mutate(PACE = "ROMO") %>% 
#   select(site, point, PACE, TotalN = `Total N\r\n %`, TotalC = `Total C\r\n %`, 
#          PMppm = `P-M\r\n ppm`, NO3Nppm = `NO3-N\r\n ppm`, NH4Nppm = `NH4-N\r\n ppm`,
#          transect = Transect)
  
d_soil_chem <- read_csv('data/MISC/FIXEDsoilsChemCNHNoProbs.csv') %>%
  parse_sample_info(quo(PLOTNUMBER)) %>% 
  select(site, point, PACE, TotalN, TotalC, PMppm, NO3Nppm, NH4Nppm, transect) %>% 
  bind_rows(romo_soil_chem) %>%
  mutate(stratum = 'none') %>% 
  left_join(d_lpi_binomial %>% select(site, point, PACE, transect, date)) %>% 
  mutate(date = if_else(is.na(date), max(date, na.rm = TRUE), date),
         C_to_N_ratio = TotalC / TotalN, availableN = NO3Nppm + NH4Nppm) %>% 
  mutate_at(vars(TotalC, TotalN), list(~ . / 100))
write_csv(d_soil_chem, 'data/MISC/CNH2018/d-soil-chemistry.csv')
d_soil_chem_long <- d_soil_chem %>% 
  select(PACE, TotalN, TotalC, PMppm, NO3Nppm, NH4Nppm, C_to_N_ratio, availableN) %>% 
  pivot_longer(-PACE, names_to = 'var', values_to = 'val')
d_soil_chem_long %>% group_by(PACE, var) %>% summarise(min = min(val), max = max(val), .groups = 'drop') %>% 
  arrange(var, PACE)
ggplot(d_soil_chem_long %>% filter(grepl('Total', var))) +
  facet_grid(PACE~var, scales = 'free') +
  geom_histogram(aes(x = val, y = ..density..), color = 'black', fill = 'white')
names(d_soil_chem)

# ---- trees info ----
d_trees_raw <- read_excel(d_path, "Tree Density and Age")
d_trees <- d_trees_raw %>% 
  parse_sample_info(quo(`Site/Point/PACE`)) %>% 
  mutate(date = ymd(Date),
         dbh_cm = as.numeric(Adult.tree.dbh.cm),
         n_saplings = as.numeric(No.Sapling.individuals), 
         n_saplings = ifelse(is.na(n_saplings), 0, n_saplings),
         stratum = 'none') %>% 
  group_by(site, point, PACE, Quadrat) %>% 
  mutate(tree_species_richness = n_distinct(Adult.tree.spp, Sapling.spp., na.rm = T)) %>% 
  ungroup() %>% 
  select(site, point, PACE, date, stratum, transect = Quadrat,
         dbh_cm, n_saplings, tree_species_richness)
# DBH is truncated, <10cm

# d_trees %>%
#   select_if(function(x) any(is.na(x))) %>%
#   summarise_each(list(~sum(is.na(.))))
# write_csv(, 'data/MISC/d-trees.csv')
write_csv(d_trees, 'data/MISC/CNH2018/d-trees.csv')


d_trees_dbh_raw <-
  read_excel('data/MISC/CNH_2018_Trees.xlsx') %>% 
  parse_sample_info(quo(`Site/Point/PACE`)) %>% 
  mutate(date = ymd(Date),
         dbh_cm = as.numeric(Adult.tree.dbh.cm),
         stratum = 'none', min_dbh = 10) %>% 
  filter(dbh_cm >= 10) %>% 
  select(site, point, PACE, date, stratum, transect = Quadrat,
         dbh_cm, min_dbh)
write_csv(d_trees_dbh_raw, 'data/MISC/CNH2018/d-trees-dbh.csv')
# d_trees_dbh_raw %>% 
#     pull(dbh_cm) %>% range(na.rm = TRUE)
# d_trees %>% filter(is.na(dbh_cm)) %>% View

hist(d_trees$dbh_cm)
hist(d_trees$n_saplings)

  