library(xtable)
library(xfun)

get_groundcover_code <- function(x) {
  sapply(x, function(z) {
    switch(z,
           G = 'grass',
           F = 'forb',
           S = 'shrub',
           T = 'tree',
           t = 'tree',  # any lower case/upper case should be combined
           M = 'moss',  # moss or lichen or soil crust
           BG = 'bare_ground',
           L = 'litter',
           `\r\nL` = 'litter',
           l = 'litter',
           R = 'rock',
           SC = 'scat',
           ST = 'scat',  # ST should be SC
           Li = 'lichen',
           LI = 'lichen',
           W = 'water',
           WATER = 'water',
           C = 'cactus',
           SNOW = 'snow',
           # NA were missing and that's a field mistake (there's always
           # groundcover, so someone forgot to record it on the datasheets and
           # we didn't have anything to put in there)
           `NA` = NA)
  })
}


get_mandate <- function(x) {
  sapply(x, function(z) {
    switch(z,
           BLM = 'MULTIUSE',
           USFSNONWILDERNESS = 'MULTIUSE',
           "USFS Non-Wilderness" = 'MULTIUSE',
           USFSWILDERNESS = 'CONSERVATION',
           "USFS Wilderness" = 'CONSERVATION',
           NPS = 'CONSERVATION'
           )
  })
}

getPACE <- function(x) {
  sapply(x, function(z) {
    switch(z,
           C0RI = 'CORI',
           CORI = 'CORI',
           Cori = 'CORI',
           cori = 'CORI',
           ROMO = 'ROMO',
           romo = 'ROMO',
           LAVO = 'LAVO',
           SEKI = 'SEKI',
           `NA` = NA
    )
  })
}

to_tex_table <- function(data, file = NULL, caption = NULL, digits = 0,
                         rownames = TRUE, width = 1, text_size = "scriptsize",
                         placement = "htbp", ...) { # row_sep = NULL
  tex_table <- xtable(data, caption = caption, digits = digits, ...)

  if (!is.null(file)) {
    print(tex_table,
          file = file, caption.placement = "top",
          include.rownames = rownames, format.args = list(big.mark = ","),
          compress = FALSE, comment = FALSE, booktabs = TRUE,
          size = text_size,
          tabular.environment = "tabularx",
          width = sprintf("%s\\textwidth", width),
          caption.width = sprintf("%s\\textwidth", width),
          table.placement = placement,
          sanitize.text.function=function(x){x}#,
          #hline.after = row_sep
    )
  } else {
    tex_table
  }
}

get_outcome_label <- function(x) {
  sapply(x, function(z) {
    switch(z,
           bare_ground = "Bare ground cover",
           C_to_N_ratio = "Carbon-to-nitrogen ratio",
           dbh_cm = "Diameter at breast height",
           forb = "Forb cover",
           grass = "Grass cover",
           n_saplings = "Number of saplings",
           PMppm = "Phosphorus PPM",
           NO3Nppm = "Nitrate PPM",
           NH4Nppm = "Ammonium PPM",
           shrub = "Shrub cover",
           TotalN = "Total nitrogen",
           TotalC = "Total carbon",
           tree = "Tree cover",
           litter = "Litter cover",
           tree_species_richness = "Tree species richness",
           FIRE.SIGN.ANY = "Fire",
           class = "Soil stability",
           HUMAN.DISTURBANCE.ANY = "Human disturbance",
           FOREST.MGMT.ANY = "Forest management",
           GRAZING.SIGN.ANY = "Grazing",
           `NA` = NA)
  })
}
