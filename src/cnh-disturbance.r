d_disturbance <- read_excel(d_path, "Summary_Disturbance") %>% 
  parse_sample_info(quo(SITE)) %>% 
  select_if(~sum(!is.na(.)) > 0) %>% 
  select(
    # Things we don't need.
    -`POINT JURISDICTION`, 
         -`BORDERING JURISDICTION`, 
         -`Comparison Mandate`,
         -SITE,
    # Things we need.
    transect = TRANSECT,
    fe_grazing_scat = CATTLE.SCAT, 
    fe_grazing_print = `CATTLE PRINT`,
    fe_fire_fallen_logs = FALLEN.LOG, 
    fe_fire_chainsaw_scars = CHAINSAW, 
    fe_fire_dirt_road = DIRT.ROAD, 
    fe_fire_fire_footprint = FIRE.FOOTPRINT)

d_disturbance %>% filter(PACE == 'CORI') %>% 
  select(starts_with('fe')) %>% cor  # what!?
  # distinct(site, point)
  # pull(`CATTLE.SCAT`) %>% hist
  
  
d_lpi_binomial %>% 
  left_join(d_disturbance %>% 
              select("site", "point", "PACE", "transect",
                     starts_with('fe'))) %>% 
  filter(PACE == 'CORI') #%>% 
pull(fe_grazing_scat)
