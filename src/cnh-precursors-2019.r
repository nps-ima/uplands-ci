library(tidyverse)
library(readxl)
library(sf)
library(mapview)
library(lubridate)

source('src/cnh-utils.r')


# PACE: protected area-centered ecosystems
#   - ROMO: Rocky Mountain National Park
#   - CORI: Colorado River (intended to be the PACE surrounding Grand Canyon)
#   - SEKI: Sequoia/Kings Canyon
#   - LAVO: Lassen
#   - ?TBD: Great Smoky Mountain
# boundary comparisons: NPS/USFS Nonwilderness; NPS/USFS Wilderness; NPS/BLM; BLM/USFS Nonwilderness; BLM USFS Wilderness; USFS Wilderness/USFS Nonwildernes

d_path_resp_data <- "data/MISC/CNH2019_MASTERDATA_June3_fixedMar1.xlsx" # excel_sheets(d_path_old)
# read_excel(d_path_old, "General Site Info") %>% parse_sample_info(quo(`Site/Point/PACE`))
d_path <- 'data/MISC/CNH2019_field_data_updated_1_12_21A.xlsx'
excel_sheets(d_path)

parse_sample_info <- function(data, sample_info_attr) {
  data %>%
    mutate(site = str_extract(!! sample_info_attr, "[:digit:]+(?=_?)"),
           point = str_extract(!! sample_info_attr, "[:alpha:]+?(?=_?)"),
           PACE = str_extract(!! sample_info_attr, "(?<=_).*"),
           PACE = getPACE(PACE))
}


d_plots <- read_excel(d_path, "CNH2019_field_data_updated_1_12") %>%
  parse_sample_info(quo(S_P_PAC)) %>%
  mutate(date = ymd(DATE), year = year(date))

d_utm <- d_plots %>%
  select(site, point, PACE, UTM_ZON, UTM_EAS, UTM_NOR) %>%
  filter(site == 96)
  # slice(c(1, 6))
crs <- "+proj=utm +zone=11 +datum=NAD83 +units=m +no_defs +ellps=GRS80 +towgs84=0,0,0"
# mapview()
mapview(st_transform(st_as_sf(d_utm, coords = c("UTM_EAS", "UTM_NOR"), crs = crs),
                     crs = 4326))

d_plot_locs <- d_plots %>%
  select(site, point, PACE, lat = Latitud, lon = Longitd) #`UTM NORTHING`
write_csv(d_plot_locs, 'data/MISC/CNH2019/d-plot-locs.csv')



source('src/cnh-utils.r')
parse_pj <- function(x) {
  sapply(x, function(.x) {
    switch(.x,
           "BLM" = "BLM" ,
           "USFS Non-Wilderness" = "USFSNONWILDERNESS",
           "USFS Wilderness" = "USFSWILDERNESS",
           "NPS" = "NPS")
  })

}

# names(d_plots)
d_plot_drivers <- d_plots %>%
  select(site, point, PACE, date, year,
         point_jurisdiction = POINT_J, #`POINT JURISDICTION`,
         bordering_jurisdiction = BORDERI, #`BORDERING JURISDICTION`,
         # point_complexity = Point_Complexity,
         # point_divergence = Point_Divergence,
         # point_dc = Point_DC,
         elev = elev_gis, slope = slope_gis, aspect = aspect_gis) %>%
  mutate(point_jurisdiction = parse_pj(point_jurisdiction),
         bordering_jurisdiction = parse_pj(bordering_jurisdiction)) %>%
  group_by(site, PACE) %>%
  mutate(elev = ifelse(elev < 0, mean(elev[elev >= 0], na.rm = TRUE), elev)) %>%
  ungroup() %>%
  mutate(#northness = cos(as.numeric(aspect) * pi / 180),
    bordering_jurisdiction = sub("Wilcerness", "Wilderness", bordering_jurisdiction),
         mandate_contrast = paste(get_mandate(point_jurisdiction),
                                  get_mandate(bordering_jurisdiction),
                                  sep = '-'),
         mandate = get_mandate(point_jurisdiction)) %>%
  # group_by(site, PACE) %>%
  # mutate(elev = ifelse(is.na(elev), mean(elev, na.rm = TRUE), elev)) %>%
  # ungroup() %>%
  select(-aspect)
# browser()
write_csv(d_plot_drivers, 'data/MISC/CNH2019/d-plot-drivers.csv')

if (FALSE) {
  d_plots %>%
    group_by(PACE) %>%
    summarise(n_points = n_distinct(point), n_sites = n_distinct(site))

  d_plots %>%
    group_by(site, point, PACE) %>%
    summarise(n_visits = n_distinct(DATE)) %>%
    ungroup()

  d_plot_drivers %>%
    group_by(point_jurisdiction, bordering_jurisdiction) %>%
    tally() %>%
    ungroup() %>%
    arrange(point_jurisdiction, bordering_jurisdiction)

  mapview(d_plots %>% filter(!is.na(Longitude)),
          xcol = "Longitude", ycol = "Latitude", crs = 4326,
          zcol = 'PACE')
}

# d_plots
d_lpi_binary <- read_excel(d_path_resp_data, "LPI Transects") %>%
  parse_sample_info(quo(`Site/Point/PACE`)) %>%
  mutate(point = ifelse(!is.na(Point) & grepl("*2$", Point), Point, point)) %>% # see "figuring out issues.xlsx"
  select(-`Site/Point/PACE`, -Site, -Point) %>%
  distinct() %>%
  gather(transect, cover, -`Line intercept distance`, -site, -point, -PACE) %>%
  mutate(transect = as.numeric(str_extract(transect, '\\d+')),
         cover = get_groundcover_code(cover)) %>%
  filter(!is.na(cover)) %>%
  distinct()

# d_lpi_binary %>% filter(site == 121, point == 'B', PACE == "LAVO") %>% View()

d_lpi_binomial <- d_lpi_binary %>%
  group_by(site, point, PACE, transect, cover) %>%
  summarise(n_hits = n()) %>%
  ungroup() %>%
  spread(cover, n_hits, fill = 0) %>%
  left_join(
    d_lpi_binary %>%
      group_by(site, point, PACE, transect) %>%
      # distinct() %>%
      summarise(n_trials = n()) %>% # was: 100
      ungroup()
  ) %>%
  left_join(d_plot_drivers %>% select(site, point, PACE, date)) %>%
  mutate(stratum = 'none')
write_csv(d_lpi_binomial %>% na.omit, 'data/MISC/CNH2019/d-lpi-binomial.csv')
d_lpi_binomial %>% filter(n_trials != 100) %>% distinct(site, point, PACE, n_trials)

# d_plot_drivers_bonus <- d_plot_drivers %>%
#   select(site, point, PACE, elev, point_jurisdiction, date, year) %>%
#   right_join(select(d_lpi_binomial, -date)) %>%
#   # filter(site == 40)
#   mutate(site = as.double(site)) %>%
#   select(site, point, PACE, transect, bare_ground, grass, shrub, tree, stratum,
#          elev, point_jurisdiction, date, year) %>%
#   na.omit
# write_csv(d_plot_drivers_bonus, 'data/MISC/CNH2019/d-plot-drivers-bonus.csv')

# ---- enhance d_plot_drivers data ----

# field_data_veg = read_excel('data/MISC/fied_data_veg.xlsx') %>%
#   parse_sample_info(quo(PLOT_NUMBE)) %>%
#   mutate(PACE = as.character(PACE))

# field_data_veg %>%
#   filter(PACE == "CORI") %>%
#   # group_by(EVT_PHYS) %>% tally %>%
#   group_by(EVT_LF) %>%
#   tally  # TODO: try this!
# field_data_evt <- field_data_veg %>%
#   mutate(
#     EVT_LF = ifelse(EVT_LF %in% c('Barren', 'Developed', 'Sparse'),
#                     'Unproductive', EVT_LF)
#   ) %>%
#   select(site, point, PACE, EVT_LF)
#
#
# d_plot_drivers %>%
#   left_join(field_data_evt) %>%
#   mutate(EVT_LF = ifelse(site == 63 & point == 'C',
#                          'Tree', EVT_LF)) #%>%
# left_join(d_lpi_binomial %>% select(site, point, PACE, ??))
# filter(PACE == 'CORI') %>%
# group_by(EVT_LF) %>% tally

# ---- disturbance info ----

d_disturbance <- read_excel(d_path_resp_data, "Disturbance") %>%
  select(where(~!all(is.na(.x)))) %>% # remove all purely NA cols
  parse_sample_info(quo(`Site/Point/PACE`)) %>%
  mutate(date_raw = date(Date), n_trials = 1,
         date = make_date(2019, month(date_raw), day(date_raw))) %>%
  rowwise() %>%
  mutate(
         FIRE.SIGN.ANY = FF, # FF: fire footprint (primarily charring, ash, etc.)
         HUMAN.DISTURBANCE.ANY = as.integer(any(RD > 0, TR > 0, CH > 0)), # TODO: add "trash"
         # HUMAN.DISTURBANCE.SUM = as.integer(RD + TR + CH), # RD: road, TR: trail, CH: chainsaw
         FOREST.MGMT.ANY = as.integer(any(CH > 0, FL > 0, FLB > 0)) # FL: fallen log, FLB: fallen log base
         # FOREST.MGMT.SUM = as.integer(CH + FL + FLB), HITS.SUM = 3
         ) %>%
         # TODO: add grazing sign and do match for 'cattle' generally
  ungroup() %>%
  select(-Site, -Point, -Notes, -`Site/Point/PACE`, -`Type...27`, -`Growth...33`,
         -`Transect Start (m)`, -`Transect End (m)`, -Date, transect = Transect,
         -date_raw) %>%
  group_by(site, point, PACE, date, transect) %>%
  summarise(across(everything(), sum), .groups = 'drop') %>%
  mutate(stratum = 'none',
         FIRE.SIGN.ANY.01 = ifelse(FIRE.SIGN.ANY > 0, 1, 0),
         HUMAN.DISTURBANCE.ANY.01 = ifelse(HUMAN.DISTURBANCE.ANY > 0, 1, 0),
         FOREST.MGMT.ANY.01 = ifelse(FOREST.MGMT.ANY > 0, 1, 0)) %>%
  left_join(d_plot_drivers %>% select(site, point, PACE, -date))
d_disturbance %>% filter(n_trials != 50) %>% distinct(site, point, PACE, n_trials)
# plot(factor(d_disturbance$FIRE.SIGN.ANY.01), d_disturbance$FIRE.SIGN.ANY)
# plot(factor(d_disturbance$HUMAN.DISTURBANCE.ANY.01), d_disturbance$HUMAN.DISTURBANCE.ANY)
# plot(factor(d_disturbance$FOREST.MGMT.ANY.01), d_disturbance$FOREST.MGMT.ANY)

# TODO: add any (not all) cattle sign (scat or print)
names(d_disturbance) <- sub(" ", ".", names(d_disturbance))
write_csv(d_disturbance %>% na.omit, 'data/MISC/CNH2019/d-disturbance.csv')

# head(d_disturbance)
# d_disturbance_signs <- na.omit(d_disturbance) %>%
#   rowwise() %>%
#          #GRAZING.SIGN.ANY = as.integer(any(CATTLE.PRINT > 0, CATTLE.SCAT > 0)),
#          #GRAZING.SIGN.ALL = as.integer(all(CATTLE.PRINT > 0, CATTLE.SCAT > 0)),
#          #GRAZING.SIGN.SUM = CATTLE.PRINT + CATTLE.SCAT,
#          #FIRE.SIGN.ANY = as.integer(any(FALLEN.LOG > 0, CHAINSAW > 0, FIRE.FOOTPRINT > 0, FALLEN.LOG.BASE)),
#          #FIRE.SIGN.ALL = as.integer(all(FALLEN.LOG > 0, CHAINSAW > 0, FIRE.FOOTPRINT > 0, FALLEN.LOG.BASE)),
#          #FIRE.SIGN.SUM = FALLEN.LOG + CHAINSAW + FIRE.FOOTPRINT + FALLEN.LOG.BASE
#   ungroup() %>%
#   select(site, point, PACE, transect, stratum, matches('FIRE|HUMAN|FOREST'), #'CATTLE|GRAZING|FIRE'
#          n_trials, date)
# write_csv(d_disturbance_signs, 'data/MISC/CNH2019/d-disturbance-signs.csv')

# d_plot_drivers %>% filter(site == 20, point == "A", PACE == "SEKI")
# d_plot_drivers_w_disturbance %>% filter(site == 20, point == "A", PACE == "SEKI")
d_plot_drivers_w_disturbance <- d_plot_drivers %>%
  select(site, point, PACE, elev, point_jurisdiction, -date, year) %>%
  right_join(d_disturbance) %>% #was: d_disturbance_signs
  mutate(site = as.double(site)) %>%
  select(site, point, PACE, transect, matches('FIRE|HUMAN|FOREST'), stratum, # was: 'CATTLE|GRAZING|FIRE'
         elev, point_jurisdiction, -date, year) %>%
  mutate(year = ifelse(year == 2010, 2019, year)) %>%
  na.omit()
write_csv(d_plot_drivers_w_disturbance, 'data/MISC/CNH2019/d-plot-drivers-w-disturbance.csv')

# # ---- soils info ----
d_soil_stability <- read_excel(d_path_resp_data, 'Soil Stability Test') %>%
  parse_sample_info(quo(`Site/Point/PACE`))
# d_soil_stability <- read_excel(d_path, 'Soilstab') %>%
#   parse_sample_info(quo(`Site/Point/PACE`))
d_soil_stability <- d_soil_stability %>%
  mutate(class = as.numeric(Class), class = ifelse(class > 6, NA, class),
         stratum = 'none', transect = `Trans #`, date = ymd(Date))
unique(d_soil_stability$class)  # Q: okay to remove 'NA'-like records
hist(d_soil_stability$class)
d_soil_stability_final <- d_soil_stability %>%
  filter(!is.na(d_soil_stability$class)) %>%
  select(site, point, PACE, date, class, stratum, transect)
write_csv(d_soil_stability_final, 'data/MISC/CNH2019/d-soil-stability.csv')
#
# # P-M ppm (which is parts per million) and percent C should be switched but just the headings
d_soil_chem <- read_excel('data/MISC/Sikes CNH5_LAVO-SEKI-GRSM Data.xlsx') %>%
  rename(PLOTINFO = `Sample Name (plot_site-transect)`) %>%
  filter(!grepl('GRSMMF', PLOTINFO)) %>%
  mutate(PLOTNUMBER = str_trim(str_extract(PLOTINFO, ".*(?=-)")),
         transect = as.integer(str_extract(PLOTINFO, "(?<=-   )\\d+"))) %>%
  parse_sample_info(quo(PLOTNUMBER)) %>%
  unnest(PACE) %>%
  select(site, point, PACE,
         TotalN = `Total N\r\n %`,
         TotalC = `Total C\r\n %`,
         PMppm = `P-M\r\n ppm`,
         # NO3Nppm, NH4Nppm,
         transect) %>%
  mutate(stratum = 'none') %>%
  left_join(d_lpi_binomial %>% select(site, point, PACE, transect, date)) %>%
  mutate(date = if_else(is.na(date), max(date, na.rm = TRUE), date),
         # availableN = NO3Nppm + NH4Nppm,
         C_to_N_ratio = TotalC / TotalN) %>%
  # pull(TotalN) %>% range
  mutate_at(vars(TotalC, TotalN), list(~ . / 100)) %>%
  filter(!is.infinite(C_to_N_ratio))
write_csv(d_soil_chem, 'data/MISC/CNH2019/d-soil-chemistry.csv')
ggplot(d_soil_chem %>%
         select(TotalN, TotalC, PMppm,
                # NO3Nppm, NH4Nppm, availableN,
                C_to_N_ratio) %>%
         gather(var, val)) +
  facet_wrap(~var, scales = 'free') +
  geom_histogram(aes(x = val, y = ..density..), color = 'black', fill = 'white')
# names(d_soil_chem)
#
# # ---- trees info ----
excel_sheets(d_path_resp_data)
# read_excel(d_path_resp_data, 'Trees') %>% View()

excel_sheets("data/MISC/CNH2019_MASTERDATA_Trees.xlsx")
d_trees_adults_raw <- read_excel("data/MISC/CNH2019_MASTERDATA_Trees.xlsx", "Adult Trees") %>% # read_excel(d_path_resp_data, 'Trees') %>%
  parse_sample_info(quo(`Site/Point/PACE`)) %>%
  mutate(date = ymd(Date),
         adult_tree_dbh_cm = as.numeric(Adult.tree.dbh.cm),
         adult_tree_spp = Adult.tree.spp,
         stratum = 'none',
         transect = Transect,
         site = as.integer(site))
d_tree_dbh <- d_trees_adults_raw %>% # DBH is truncated, <10cm
  select(PACE, site, point, date, transect, stratum, #quadrat = quadrat_adults,
         adult_tree_spp, dbh_cm = adult_tree_dbh_cm) %>%
  filter(!is.na(dbh_cm)) %>%
  mutate(min_dbh = ifelse(dbh_cm < 10, 0, 10), year = year(date))
write_csv(d_tree_dbh, 'data/MISC/CNH2019/d-trees-dbh.csv')

d_trees_saplings_raw <- read_excel("data/MISC/CNH2019_MASTERDATA_Trees.xlsx", "Sapling Trees") %>% # read_excel(d_path_resp_data, 'Trees') %>%
  parse_sample_info(quo(`Site/Point/PACE`)) %>%
  mutate(date = ymd(Date),
         sapling_spp = Sapling.spp.,
         n_saplings = as.integer(No.Sapling.individuals),
         stratum = 'none',
         # transect = 999,
         transect = Transect)
d_trees_saplings_tmp <- d_trees_saplings_raw %>% # DBH is truncated, <10cm
  select(PACE, site, point, date, transect, stratum, #quadrat = quadrat_adults,
         n_saplings, sapling_spp) %>%
  mutate(site = as.integer(site))
# d_trees_saplings$n_saplings %>% range
# d_trees_saplings$site %>% unique()
d_trees_saplings <- d_plot_drivers_w_disturbance %>%
  select(site, point, PACE, transect, year, stratum) %>%
  left_join(d_trees_saplings_tmp) %>%
  mutate(n_saplings = ifelse(is.na(n_saplings), 0, n_saplings),
         date = if_else(is.na(date), ymd(make_date(year, 6, 15)), date)) #%>%
d_trees_saplings
write_csv(d_trees_saplings, 'data/MISC/CNH2019/d-trees-saplings.csv')

d_tree <- bind_rows(d_trees_saplings %>% rename(spp = sapling_spp),
          d_tree_dbh %>% rename(spp = adult_tree_spp)) %>%
  group_by(site, point, PACE, transect, year) %>% # was Quadrat
  mutate(tree_species_richness = n_distinct(spp, na.rm = T),
         n_saplings = sum(n_saplings, na.rm = T)) %>%
  ungroup()
write_csv(d_tree, 'data/MISC/CNH2019/d-trees.csv')
  # pull(n_saplings) %>% table()

# d_tree_saplings <- d_trees_raw %>%
#   select(PACE, site, point, date, quadrat = transect, #quadrat = quadrat_saplings,
#          sapling_spp, n_saplings) %>%
#   filter(!is.na(n_saplings))
# write_csv(d_tree_saplings, 'data/MISC/CNH2019/d-trees-saplings.csv')

# d_tree <- d_trees_raw %>%
  # group_by(site, point, PACE, transect) %>% # was Quadrat
  # mutate(tree_species_richness = n_distinct(Adult.tree.spp, Sapling.spp., na.rm = T),
  #        n_saplings = sum(n_saplings)) %>%
  # ungroup() %>%
#   select(site, point, PACE, date, stratum, quadrat = transect, # quadrat = Quadrat,
#          n_saplings, tree_species_richness) %>%
#   filter(!is.na(n_saplings), !is.na(tree_species_richness))
# write_csv(d_tree, 'data/MISC/CNH2019/d-trees.csv')

# ---- new spatial coordinates stuff! -----

library(tidyverse)
library(readxl)
library(sf)
source('src/cnh-utils.r')

# read_excel('data/MISC/seki_lavo_field_data_6_2_21.xlsx')
# read_excel('data/MISC/field_data_AlbersNAD1983_full.xlsx')


d_coords1 <- read_excel('data/MISC/seki_lavo_field_data_6_2_21.xlsx') %>%
  parse_sample_info(quo(S_P_PAC)) %>% 
  select(site_id = site, point = point, unit_code = PACE, x = xcoords,
         y = ycoords, jurisdiction = POINT_J)

d_coords2 <- read_excel('data/MISC/field_data_AlbersNAD1983_full.xlsx') %>%
  parse_sample_info(quo(PLOT_NUMBE)) %>% 
  select(site_id = site, point = point, unit_code = PACE, x = x_coord,
         y = y_coord, jurisdiction = JURISDICTI) %>% 
  bind_rows(
    tibble(site_id = "40", point = "A", unit_code = "CORI",
           x = -1658990, y = 1625600, jurisdiction = "BLM"),
    tibble(site_id = "42", point = "D", unit_code = "ROMO",
           x = -828027.8, y = 1956206, jurisdiction = "USFS NONWILDERNESS"),
    tibble(site_id = "63", point = "C", unit_code = "CORI",
           x = -1419671, y = 1594820, jurisdiction = "USFS NONWILDERNESS")
  ) %>% 
  mutate(jurisdiction = ifelse(site_id == "54" & unit_code == "CORI" & jurisdiction == "USFS", 
                               "USFS Non-Wilderness", jurisdiction),
         jurisdiction = ifelse(jurisdiction == "USFS NONWILDERNESS", 
                               "USFS Non-Wilderness", jurisdiction),
         jurisdiction = ifelse(jurisdiction == "USFS WILDERNESS", 
                               "USFS Wilderness", jurisdiction))

d_coords <- bind_rows(d_coords1, d_coords2) %>% distinct()
write_csv(d_coords, 'data/MISC/d-coords-combined.csv')

# sf_coords <- st_transform(st_as_sf(d_coords, coords = c("x", "y"),
#                                    crs = 5070),
#                           crs = 4326)



# st_transform(st_sfc(st_point(c(-112.03447, 36.32171)), crs = 4326),
#              crs = 5070)

# mapview::mapview(sf_coords %>% filter(site_id == '63', unit_code == "CORI"))
# d_coords %>% group_by(site_id, unit_code) %>% tally() %>% filter(n != 4)
# d_coords %>% filter(site_id == '10', unit_code == "ROMO")
