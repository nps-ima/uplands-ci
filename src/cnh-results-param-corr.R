function() {
  # folder <- "output/MISC/LAVO/ground-cover-new/bare-ground-cover/od-binom_inv-logit_b0_fixed-stratum-var_rand-point_jurisdiction-effects/elev_point_jurisdiction-deflections_fire.sign.any_human.disturbance.any_forest.mgmt.any"
  folder <- "output/MISC/LAVO/disturbance-new/fire/od-binom_inv-logit_b0_hier-site-var_rand-point_jurisdiction-effects/elev_point_jurisdiction-deflections"
  mcarray_list <- readRDS(file.path(folder, '99-misc', 'z-jags.rds'))
  # resp_var_desc <- readRDS(file.path(folder, '00-input', 'jags-info.rds'))$description
  
  mcarray_list$rho.Beta %>% str
  read_csv(file.path(folder, '00-input', 'encoding-for-point_jurisdiction.csv')) %>% 
    arrange(point_jurisdiction)
  
  summary(mcarray_list$rho.Beta, FUN = 'mean')$stat
  summary(mcarray_list$sigma.Beta, FUN = 'mean')$stat
  
  str(mcarray_list$sigma.Beta)
  hist(mcarray_list$sigma.Beta[1, 1, , ])
  
}
