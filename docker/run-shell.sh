#!/usr/bin/env bash

docker run -it --rm \
    -v "$(pwd)":/content \
    -w /content \
    cspinc/nps-ima:uplands-ci \
    /bin/bash
