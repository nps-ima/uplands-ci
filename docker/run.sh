#!/usr/bin/env bash

TAG=${1:-latest}
PORT=${2:-1607}

REPO=$(basename $(pwd))
IMAGE=cspinc/nps-ima/$REPO:$TAG

if [[ "$(docker images -q $IMAGE 2> /dev/null)" == "" ]]; then
  echo "Image not found, building from recipe...."
  ./$(find . -path \*build.sh) $TAG
fi

echo "http://localhost:$PORT/ (with usr and pwd '$REPO')"

rm -rf kitematic/ .rstudio/ rstudio/
docker run -it --rm \
		--name rstudio-$REPO-$TAG-$PORT \
		-v "$(pwd)":/home/$REPO \
		-w /home/$REPO \
		-e USER=$REPO \
		-e PASSWORD=$REPO \
		-p $PORT:8787 \
		$IMAGE

rm -rf kitematic/ .rstudio/ rstudio/
