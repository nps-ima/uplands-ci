#!/usr/bin/env bash

docker run -ti --rm \
  --name gsutil-$(basename $(pwd)) \
  -v ~/google-cloud-sdk/csp-projects:/root/.config/gcloud \
  -v $(pwd):/home \
  -w /home \
  google/cloud-sdk:latest \
  /bin/bash
