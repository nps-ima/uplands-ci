#!/usr/bin/env Rscript

"
A pipeline for converting the details of an analysis stored in a configuration
file into an actual analysis

Usage:
analysis-pipeline.r [-f <file>] [-a <adaptation_iters>] [-u <burnin_iters>] [-n <iterations>] [-b <bgcolor>] [-c <n_cores>] [-t <test>] [-m <hpc>] [-d <dq_override>] [-l <bad_logs>] [-x <excl_null_mods>]

Options:
-f The YAML file for which an analysis is desired
-a The number of iterations for adaptation [default: 3000]
-u The number of iterations for updating [default: 15000]
-n The number of iterations of the Markov chain to run [default: 5000]
-b The background color of plots [default: default]
-c The number of cores for parallelized analyses [default: 1]
-t Submit the job as a test by running up to two analyses at random [default: FALSE]
-m Create precursors for use on HPC [default: FALSE]
-d Override (block) derived quantities [default: FALSE]
-l Bad runs filelist [default: none]
-x Drop null (time-only) models (running only jobs with covariates) [default: FALSE]
" -> doc

if (!interactive()) {
  args <- docopt::docopt(doc)
  yaml_file <- args$f
  n_adapt <- as.numeric(args$a)
  n_update <- as.numeric(args$u)
  n_iter <- as.numeric(args$n)
  plot_bgcolor <- as.character(args$b)
  n_cores <- as.numeric(args$c)
  submit_as_test <- as.logical(args$t)
  prep_for_hpc <- as.logical(args$m)
  excl_null_mods <- as.logical(args$x)
  override_dq <- as.logical(args$d)
  bad_logs <- as.character(args$l)
} else {
  yaml_file <-  # <<------------------------------------------------- CUSTOMIZE!

  # Misc.
  # './config/MISC/CORI/ground-cover.yml'
  # './config/MISC/ROMO/ground-cover.yml'
  # './config/MISC/LAVO/ground-cover.yml'
  # './config/MISC/SEKI/ground-cover.yml'
  # './config/MISC/CORI/ground-cover-new.yml'
  # './config/MISC/ROMO/ground-cover-new.yml'
  # './config/MISC/LAVO/ground-cover-new.yml'
  # './config/MISC/SEKI/ground-cover-new.yml'
  # './config/MISC/ROMO/soil-stability.yml'
  # './config/MISC/CORI/soil-stability.yml'
  # './config/MISC/LAVO/soil-stability.yml'
  # './config/MISC/SEKI/soil-stability.yml'
  # './config/MISC/ROMO/soil-stability-new.yml'
  # './config/MISC/CORI/soil-stability-new.yml'
  # './config/MISC/LAVO/soil-stability-new.yml'
  # './config/MISC/SEKI/soil-stability-new.yml'
  # './config/MISC/CORI/counts.yml'
  # './config/MISC/ROMO/counts.yml'
  # './config/MISC/LAVO/counts.yml'
  # './config/MISC/SEKI/counts.yml'
  # './config/MISC/CORI/counts-new.yml'
  # './config/MISC/ROMO/counts-new.yml'
  # './config/MISC/LAVO/counts-new.yml'
  # './config/MISC/SEKI/counts-new.yml'
  # './config/MISC/CORI/dbh.yml'
  # './config/MISC/ROMO/dbh.yml'
  # './config/MISC/LAVO/dbh.yml'
  # './config/MISC/SEKI/dbh.yml'
  # './config/MISC/CORI/dbh-new.yml'
  # './config/MISC/ROMO/dbh-new.yml'
  # './config/MISC/LAVO/dbh-new.yml'
  # './config/MISC/SEKI/dbh-new.yml'
  # './config/MISC/CORI/soil-chemistry-percent.yml'
  # './config/MISC/ROMO/soil-chemistry-percent.yml' # this data does not exist!!
  # './config/MISC/LAVO/soil-chemistry-percent.yml'
  # './config/MISC/SEKI/soil-chemistry-percent.yml'
  # './config/MISC/CORI/soil-chemistry-percent-new.yml'
  # './config/MISC/ROMO/soil-chemistry-percent-new.yml'
  # './config/MISC/LAVO/soil-chemistry-percent-new.yml'
  # './config/MISC/SEKI/soil-chemistry-percent-new.yml'
  # './config/MISC/CORI/soil-chemistry-ppm.yml'
  # './config/MISC/ROMO/soil-chemistry-ppm.yml'
  # './config/MISC/LAVO/soil-chemistry-ppm.yml'
  # './config/MISC/SEKI/soil-chemistry-ppm.yml'
  # './config/MISC/CORI/soil-chemistry-ppm-new.yml'
  # './config/MISC/ROMO/soil-chemistry-ppm-new.yml'
  # './config/MISC/LAVO/soil-chemistry-ppm-new.yml'
  # './config/MISC/SEKI/soil-chemistry-ppm-new.yml'
  # './config/MISC/CORI/disturbance.yml'
  # './config/MISC/ROMO/disturbance.yml'
  # './config/MISC/LAVO/disturbance.yml'
  # './config/MISC/SEKI/disturbance.yml'
  # './config/MISC/CORI/disturbance-new.yml'
  # './config/MISC/ROMO/disturbance-new.yml'
  # './config/MISC/LAVO/disturbance-new.yml'
  # './config/MISC/SEKI/disturbance-new.yml'
  # './config/SODN/ORPI/pa-bern.yml'
  # './config/CDPR/TSRA/cwd.yml'
  # './config/CDPR/TSRA/fine-fuels.yml'
  # './config/CDPR/TSRA/forest-structure.yml'
  # './config/CDPR/TSRA/diversity-richness.yml'
  # './config/CDPR/TSRA/diversity-evenness.yml'
  # './config/CDPR/TSRA/duff-litter.yml'
  # './config/CDPR/TSRA/tree-size.yml'
  # './config/CDPR/TSRA/ground-cover.yml'

  # Gaps.
  # './config/NCPN/CARE/canopy-gaps-thin.yml'
  # './config/NCPN/CARE/canopy-gaps-naive-thin.yml'
  # './config/NCPN/CARE/canopy-gaps-2021.yml' # just hdeep!
  # './config/NCPN/CARE/canopy-gaps-naive-2021.yml'
  # './config/NCPN/CARE/basal-gaps.yml'
  # './config/SCPN/PEFO/gap-size.yml'
  './config/NCPN/DINO2/canopy-gaps.yml'

  # Ocular cover (estimated on a continuous scale)...
  # './config/ROMN/LIBI/ocular-cover.yml'

  # Point intercept data...
  # './config/SODN/FOBO/pi-soil-bb.yml'
  # './config/SOPN/BEOL/pi-cover-bb.yml'
  # './config/NCPN/DINO/pi-cover-zibb.yml'
  # './config/SOPN/BEOL/pi-cover-zibb.yml'
  # './config/ROMN/GRKO/veg-categories-rel-cover.yml'
  # './config/ROMN/GRKO/veg-categories-rel-cover-no-zone.yml'
  # './config/ROMN/LIBI/veg-categories-abs-cover.yml'
  # './config/ROMN/LIBI/veg-categories-simple-abs-cover-2019.yml'
  # './config/ROMN/LIBI/veg-categories-rel-cover.yml'
  # './config/ROMN/GRKO/surface-cover.yml'
  # './config/NCPN/CARE/point-intercept-cover.yml'
  # './config/NCPN/CARE/pi-cover-bb.yml'
  # './config/NCPN/CARE/pi-cover-zib.yml'
  # './config/NCPN/CARE/point-intercept-cover-zibb-final.yml'
  # './config/NCPN/CARE/ex-freq.yml'
  # './config/NCPN/DINO/ex-freq.yml'
  # './config/ROMN/GRKO/freq.yml'
  # './config/SODN/ORPI/lpi-nurse.yml'
  # './config/SODN/ORPI/bsc_quads.yml'

  # Species richness...
  # './config/SOPN/BEOL/species-richness.yml'
  # './config/ROMN/LIBI/species-richness.yml'
  # './config/ROMN/LIBI/species-richness-tmp.yml'
  # './config/ROMN/LIBI/rich-vs-exotics-2019.yml'
  # './config/ROMN/GRKO/species-richness.yml'
  # './config/ROMN/GRKO/species-richness-no-zone.yml'
  # './config/ROMN-deprec/Other/species-richness.yml'
  # './config/SCPN-0/PEFO/species-richness.yml'
  # './config/SCPN/PEFO_C/rich.yml'
  # './config/SCPN/PEFO_S/rich.yml'
  # './config/SCPN/CHCU_S/rich.yml'

  # Other densities....
  # './config/NCPN/DINO/shrub-density.yml'
  # './config/NCPN/CARE/shrub-density.yml'

  # Cover class...
  # './config/SCPN/BAND/cover-class.yml'
  # './config/SCPN/WUPA_S/cc-fxn.yml'
  # './config/SCPN/PEFO_S/cover-class.yml'
  # './config/SCPN/PEFO_S/cover-class-spp.yml'
  # './config/SCPN/CHCU_S/cc-fxn.yml'
  # './config/SCPN/CHCU_S/cc-spp-redux.yml'
  # './config/SCPN/CHCU_S/cc-spp.yml'
  # './config/SCPN/PEFO_S/cover-class-spp.yml'
  './config/SCPN2/GRASS/cc-fxn.yml'

  # Soil stability...
  # './config/ROMN/LIBI/soil-stability.yml'
  # './config/ROMN/GRKO/soil-stability.yml'
  # './config/NCPN/CARE/soil-stability.yml'
  # './config/NCPN/DINO/soil-stability.yml'
  # './config/SCPN/PEFO/soil-stability.yml'
  # './config/SODN/ORPI/soil-stability-JABES.yml'

  n_adapt <- 3000#as.numeric(1500)
  n_update <- 8000#as.numeric(3500)
  n_iter <- 5000#as.numeric(2500)
  plot_bgcolor <- 'default'  # 'default' or 'darkunica'
  n_cores <- 1
  submit_as_test <- FALSE
  prep_for_hpc <- FALSE
  excl_null_mods <- F # grepl('MISC|CDPR', yaml_file)  #FALSE
  override_dq <- F#grepl('CDPR/TSRA', yaml_file)  # FALSE
  bad_logs <- "none"
}

# ==== begin pipeline ===========================================================
source('src/get-config.r')  # TODO: reduce redundancy in this call....
source('src/apply-config.r')
source('src/theme.r')
source('src/serializers.r')
source('src/fit.r')
rjags::load.module('dic')
rjags::load.module('glm')
check_packages('MCMCpack')

# logs/killed_log_paths
config_list <- get_config(yaml_file)
analysis_scenarios <- config_list$analysis_scenarios

if(n_cores > 1) { source('src/cluster.r'); cl <- cluster_init(n_cores) }
if(interactive()) {
  # warning(paste('You are about to run', nrow(analysis_scenarios),
  #               'analyses. Plan accordingly (or slice)!'))
  warning(
    sprintf('Prior to any added filters below, you are about to run %s jobs.',
            nrow(analysis_scenarios %>%
                   {`if`(excl_null_mods, filter(., !is.na(additional_covariates)), .)}))
  )

}

# key_vars <- c(
#   "response", "hits", "trials", "likelihood", "additional_covariates",
#   "deterministic_model", "group_level_effects", "group_level_effects_zeros",
#   "parameterization", "add_rand_coefs", "var_level", "var_type"
# )
# reruns <- read_csv('sandbox/reruns.csv') %>%
#   select(any_of(key_vars)) %>%
#   mutate(rerun = TRUE)
# analysis_scenarios_tmp <- analysis_scenarios %>%
#   left_join(reruns)
# #
# # # analysis_scenarios_tmp <- analysis_scenarios %>%
# # #   mutate(rerun = grepl('bare_ground', hits))


this_slice <-  # <<-------------------------------------------------- CUSTOMIZE!
  # which(analysis_scenarios$response == "medium_trees_per_plot")
  8#1:nrow(analysis_scenarios)#which(analysis_scenarios_tmp$rerun)
if (prep_for_hpc) this_slice <- 1:nrow(analysis_scenarios)
analysis_summary <- analysis_scenarios %>%
  mutate(scenario = 1:n()) %>%
  {`if`(excl_null_mods, filter(., !is.na(additional_covariates)), .)} %>%
  {`if`(interactive(), filter(., scenario %in% this_slice), .)} %>%
  {`if`(submit_as_test, sample_n(., min(c(2, nrow(.)))), .)} %>%
  # If n_cores > 1, then multidplyr handles each call to `rowwise_fit`, sending
  # each analysis scenario (row) to a different core (CPU), otherwise it just
  # takes each row one at a time (serially) using `rowwise`.
  group_by(scenario) %>%
  {`if`(n_cores > 1,
        partition(., cluster=cl),
        .)} %>%
  do(rowwise_fit(.,
                 yaml_info = config_list$yaml_info,
                 yaml_branches = config_list$yaml_branches,
                 save_full_jags_obj = FALSE, #grepl('CDPR/TSRA', yaml_file),
                 create_filelist = prep_for_hpc,
                 bad_logs = bad_logs,
                 destroy_xy = TRUE)) %>%
  {`if`(n_cores > 1, collect(.), ungroup(.))}
#
#
